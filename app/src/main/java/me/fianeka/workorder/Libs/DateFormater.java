package me.fianeka.workorder.Libs;

import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by galgadot on 01/02/18.
 */

public class DateFormater {

    public static String doFormat(String type,String dater){
        SimpleDateFormat enFormat = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat inFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String[] days = new String[] {"", "Ahad", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu" };
        String[] month = new String[] {"Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli","Agustus","September","Oktober","November","Desember" };

        try {
            Date date;
            if (type.equals("en")){
                date = enFormat.parse(dater);
            }else{
                date = inFormat.parse(dater);
            }
            Calendar cal=Calendar.getInstance();
            cal.setTime(date);
            String res = days[cal.get(Calendar.DAY_OF_WEEK)]
                        +", "
                        +String.valueOf(cal.get(Calendar.DATE))
                        +" "
                        +month[cal.get(Calendar.MONTH)]
                        +" "
                        +cal.get(Calendar.YEAR);
            return res;
        }catch (Exception ex){
            Log.d("Eror", ex.getMessage());
            return ex.getMessage();
        }
    }
}
