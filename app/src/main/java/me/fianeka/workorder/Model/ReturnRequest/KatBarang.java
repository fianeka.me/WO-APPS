package me.fianeka.workorder.Model.ReturnRequest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import me.fianeka.workorder.Model.Data.KategoriBarang;
import me.fianeka.workorder.Model.Data.SubdivisiOrder;

/**
 * Created by galgadot on 28/01/18.
 */

public class KatBarang {

    @SerializedName("result")
    @Expose
    private Boolean result;

    @SerializedName("data")
    @Expose
    private List<KategoriBarang> data;

    @SerializedName("message")
    @Expose
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getResult() {
        return result;
    }

    public void setResult(Boolean result) {
        this.result = result;
    }

    public List<KategoriBarang> getData() {
        return data;
    }

    public void setData(List<KategoriBarang> data) {
        this.data = data;
    }
}
