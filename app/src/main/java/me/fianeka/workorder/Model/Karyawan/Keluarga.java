package me.fianeka.workorder.Model.Karyawan;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by galgadot on 29/01/18.
 */

public class Keluarga {

    @SerializedName("id_kel")
    @Expose
    private String idkel;

    @SerializedName("nama")
    @Expose
    private String nama;

    @SerializedName("hubungan")
    @Expose
    private String hubungan;

    @SerializedName("tgl_lahirkel")
    @Expose
    private String tgllahir;

    @SerializedName("jenkel")
    @Expose
    private String jenkel;

    @SerializedName("alamat_kel")
    @Expose
    private String alamat;

    @SerializedName("pekerjaankel")
    @Expose
    private String pekerjaan;

    public String getPekerjaan() {
        return pekerjaan;
    }

    public void setPekerjaan(String pekerjaan) {
        this.pekerjaan = pekerjaan;
    }

    public String getIdkel() {
        return idkel;
    }

    public void setIdkel(String idkel) {
        this.idkel = idkel;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getHubungan() {
        return hubungan;
    }

    public void setHubungan(String hubungan) {
        this.hubungan = hubungan;
    }

    public String getTgllahir() {
        return tgllahir;
    }

    public void setTgllahir(String tgllahir) {
        this.tgllahir = tgllahir;
    }

    public String getJenkel() {
        return jenkel;
    }

    public void setJenkel(String jenkel) {
        this.jenkel = jenkel;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }
}
