package me.fianeka.workorder.Model.Karyawan;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by galgadot on 29/01/18.
 */

public class Kursus {

    @SerializedName("id_kur")
    @Expose
    private String idkur;

    @SerializedName("tema")
    @Expose
    private String tema;

    @SerializedName("penyelenggara")
    @Expose
    private String penyelenggara;

    @SerializedName("tgl_kursus")
    @Expose
    private String tglkursus;

    @SerializedName("sertifikat")
    @Expose
    private String sertifikat;

    public String getIdkur() {
        return idkur;
    }

    public void setIdkur(String idkur) {
        this.idkur = idkur;
    }

    public String getTema() {
        return tema;
    }

    public void setTema(String tema) {
        this.tema = tema;
    }

    public String getPenyelenggara() {
        return penyelenggara;
    }

    public void setPenyelenggara(String penyelenggara) {
        this.penyelenggara = penyelenggara;
    }

    public String getTglkursus() {
        return tglkursus;
    }

    public void setTglkursus(String tglkursus) {
        this.tglkursus = tglkursus;
    }

    public String getSertifikat() {
        return sertifikat;
    }

    public void setSertifikat(String sertifikat) {
        this.sertifikat = sertifikat;
    }
}
