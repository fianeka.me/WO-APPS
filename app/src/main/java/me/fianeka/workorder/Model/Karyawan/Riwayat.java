package me.fianeka.workorder.Model.Karyawan;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by galgadot on 29/01/18.
 */

public class Riwayat {

    @SerializedName("id_riwayat")
    @Expose
    private String idriwayat;

    @SerializedName("pendidikan")
    @Expose
    private String pendidikan;

    @SerializedName("institusi")
    @Expose
    private String institusi;

    @SerializedName("tahun")
    @Expose
    private String tahun;

    public String getIdriwayat() {
        return idriwayat;
    }

    public void setIdriwayat(String idriwayat) {
        this.idriwayat = idriwayat;
    }

    public String getPendidikan() {
        return pendidikan;
    }

    public void setPendidikan(String pendidikan) {
        this.pendidikan = pendidikan;
    }

    public String getInstitusi() {
        return institusi;
    }

    public void setInstitusi(String institusi) {
        this.institusi = institusi;
    }

    public String getTahun() {
        return tahun;
    }

    public void setTahun(String tahun) {
        this.tahun = tahun;
    }
}
