package me.fianeka.workorder.Model.ReturnRequest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import me.fianeka.workorder.Model.Data.KategoriBarang;
import me.fianeka.workorder.Model.Karyawan.ProfileData;

/**
 * Created by galgadot on 28/01/18.
 */

public class Profile {

    @SerializedName("result")
    @Expose
    private Boolean result;

    @SerializedName("data")
    @Expose
    private ProfileData data;

    @SerializedName("message")
    @Expose
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getResult() {
        return result;
    }

    public void setResult(Boolean result) {
        this.result = result;
    }

    public ProfileData getData() {
        return data;
    }

    public void setData(ProfileData data) {
        this.data = data;
    }
}
