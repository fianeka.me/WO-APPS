package me.fianeka.workorder.Model.Data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import me.fianeka.workorder.Model.Karyawan.Karyawan;

/**
 * Created by galgadot on 30/01/18.
 */

public class HistoryBon {

    @SerializedName("id_bon")
    @Expose
    private String idbon;

    @SerializedName("tgl_pinjam")
    @Expose
    private String tglpinjam;

    @SerializedName("jumlahpinjam")
    @Expose
    private String jumlahpinjam;

    @SerializedName("nama_barang")
    @Expose
    private String namabarang;

    public String getIdbon() {
        return idbon;
    }

    public void setIdbon(String idbon) {
        this.idbon = idbon;
    }

    public String getTglpinjam() {
        return tglpinjam;
    }

    public void setTglpinjam(String tglpinjam) {
        this.tglpinjam = tglpinjam;
    }

    public String getJumlahpinjam() {
        return jumlahpinjam;
    }

    public void setJumlahpinjam(String jumlahpinjam) {
        this.jumlahpinjam = jumlahpinjam;
    }

    public String getNamabarang() {
        return namabarang;
    }

    public void setNamabarang(String namabarang) {
        this.namabarang = namabarang;
    }
}
