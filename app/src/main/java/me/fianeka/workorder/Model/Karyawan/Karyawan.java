package me.fianeka.workorder.Model.Karyawan;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by galgadot on 28/01/18.
 */

public class Karyawan {

    @SerializedName("nik")
    @Expose
    private String nik;

    @SerializedName("nama")
    @Expose
    private String nama;

    @SerializedName("tgl_lahir")
    @Expose
    private String tgl_lahir;

    @SerializedName("tempat_lahir")
    @Expose
    private String tempat_lahir;

    @SerializedName("tgl_mulaikerja")
    @Expose
    private String tgl_mulaikerja;

    @SerializedName("statuskaryawan")
    @Expose
    private String statuskaryawan;

    @SerializedName("agama")
    @Expose
    private String agama;

    @SerializedName("status_diri")
    @Expose
    private String status;

    @SerializedName("kewarganegaraan")
    @Expose
    private String kewarganegaraan;


    @SerializedName("pendidikan")
    @Expose
    private String pendidikan;

    @SerializedName("jekel")
    @Expose
    private String jekel;

    @SerializedName("photo")
    @Expose
    private String photo;

    @SerializedName("alamat")
    @Expose
    private String alamat;

    @SerializedName("notel")
    @Expose
    private String notel;

    @SerializedName("id_jabatan")
    @Expose
    private String id_jabatan;

    @SerializedName("namajabatan")
    @Expose
    private String jabatan;

    @SerializedName("id_subdiv")
    @Expose
    private String id_subdiv;

    @SerializedName("namasubdiv")
    @Expose
    private String subdivisi;

    @SerializedName("id_div")
    @Expose
    private String id_div;

    @SerializedName("namadivisi")
    @Expose
    private String divisi;


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getNik() {
        return nik;
    }

    public void setNik(String nik) {
        this.nik = nik;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getTgl_lahir() {
        return tgl_lahir;
    }

    public void setTgl_lahir(String tgl_lahir) {
        this.tgl_lahir = tgl_lahir;
    }

    public String getTempat_lahir() {
        return tempat_lahir;
    }

    public void setTempat_lahir(String tempat_lahir) {
        this.tempat_lahir = tempat_lahir;
    }

    public String getTgl_mulaikerja() {
        return tgl_mulaikerja;
    }

    public void setTgl_mulaikerja(String tgl_mulaikerja) {
        this.tgl_mulaikerja = tgl_mulaikerja;
    }

    public String getStatuskaryawan() {
        return statuskaryawan;
    }

    public void setStatuskaryawan(String statuskaryawan) {
        this.statuskaryawan = statuskaryawan;
    }

    public String getAgama() {
        return agama;
    }

    public void setAgama(String agama) {
        this.agama = agama;
    }

    public String getKewarganegaraan() {
        return kewarganegaraan;
    }

    public void setKewarganegaraan(String kewarganegaraan) {
        this.kewarganegaraan = kewarganegaraan;
    }

    public String getPendidikan() {
        return pendidikan;
    }

    public void setPendidikan(String pendidikan) {
        this.pendidikan = pendidikan;
    }

    public String getJekel() {
        return jekel;
    }

    public void setJekel(String jekel) {
        this.jekel = jekel;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getNotel() {
        return notel;
    }

    public void setNotel(String notel) {
        this.notel = notel;
    }

    public String getId_jabatan() {
        return id_jabatan;
    }

    public void setId_jabatan(String id_jabatan) {
        this.id_jabatan = id_jabatan;
    }

    public String getJabatan() {
        return jabatan;
    }

    public void setJabatan(String jabatan) {
        this.jabatan = jabatan;
    }

    public String getId_subdiv() {
        return id_subdiv;
    }

    public void setId_subdiv(String id_subdiv) {
        this.id_subdiv = id_subdiv;
    }

    public String getSubdivisi() {
        return subdivisi;
    }

    public void setSubdivisi(String subdivisi) {
        this.subdivisi = subdivisi;
    }

    public String getId_div() {
        return id_div;
    }

    public void setId_div(String id_div) {
        this.id_div = id_div;
    }

    public String getDivisi() {
        return divisi;
    }

    public void setDivisi(String divisi) {
        this.divisi = divisi;
    }
}
