package me.fianeka.workorder.Model.Data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import me.fianeka.workorder.Model.Karyawan.Karyawan;

/**
 * Created by galgadot on 30/01/18.
 */

public class HistoryOrder {

    @SerializedName("id_order")
    @Expose
    private String idorder;

    @SerializedName("tgl_pemesanan")
    @Expose
    private String tglpemesanan;

    @SerializedName("keterangan")
    @Expose
    private String keterangan;

    @SerializedName("status")
    @Expose
    private String status;

    @SerializedName("namajenisorder")
    @Expose
    private String namajenisorder;

    @SerializedName("action")
    @Expose
    private String action;

    @SerializedName("favicon")
    @Expose
    private String favicon;

    @SerializedName("kordinator")
    @Expose
    private List<Karyawan> kordinator;

    public String getIdorder() {
        return idorder;
    }

    public void setIdorder(String idorder) {
        this.idorder = idorder;
    }

    public String getTglpemesanan() {
        return tglpemesanan;
    }

    public void setTglpemesanan(String tglpemesanan) {
        this.tglpemesanan = tglpemesanan;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getNamajenisorder() {
        return namajenisorder;
    }

    public void setNamajenisorder(String namajenisorder) {
        this.namajenisorder = namajenisorder;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getFavicon() {
        return favicon;
    }

    public void setFavicon(String favicon) {
        this.favicon = favicon;
    }

    public List<Karyawan> getKordinator() {
        return kordinator;
    }

    public void setKordinator(List<Karyawan> kordinator) {
        this.kordinator = kordinator;
    }
}
