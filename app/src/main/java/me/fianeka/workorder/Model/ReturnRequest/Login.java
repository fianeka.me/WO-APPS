package me.fianeka.workorder.Model.ReturnRequest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import me.fianeka.workorder.Model.Karyawan.Karyawan;

/**
 * Created by galgadot on 28/01/18.
 */

public class Login {

    @SerializedName("result")
    @Expose
    private Boolean result;

    @SerializedName("data")
    @Expose
    private List<Karyawan> data;

    @SerializedName("message")
    @Expose
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getResult() {
        return result;
    }

    public void setResult(Boolean result) {
        this.result = result;
    }

    public List<Karyawan> getData() {
        return data;
    }

    public void setData(List<Karyawan> data) {
        this.data = data;
    }

}
