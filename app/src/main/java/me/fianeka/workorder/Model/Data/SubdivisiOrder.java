package me.fianeka.workorder.Model.Data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by galgadot on 28/01/18.
 */

public class SubdivisiOrder {

    @SerializedName("idkor")
    @Expose
    private String idkordinator;

    @SerializedName("id_subdiv")
    @Expose
    private String idsubdiv;

    @SerializedName("namasubdiv")
    @Expose
    private String subdivisi;

    @SerializedName("id_div")
    @Expose
    private String iddivisi;

    @SerializedName("keterangansubdiv")
    @Expose
    private String keterangan;

    @SerializedName("img_holder")
    @Expose
    private String photo;

    public String getIdkordinator() {
        return idkordinator;
    }

    public void setIdkordinator(String idkordinator) {
        this.idkordinator = idkordinator;
    }

    public String getIdsubdiv() {
        return idsubdiv;
    }

    public void setIdsubdiv(String idsubdiv) {
        this.idsubdiv = idsubdiv;
    }

    public String getSubdivisi() {
        return subdivisi;
    }

    public void setSubdivisi(String subdivisi) {
        this.subdivisi = subdivisi;
    }

    public String getIddivisi() {
        return iddivisi;
    }

    public void setIddivisi(String iddivisi) {
        this.iddivisi = iddivisi;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }
}
