package me.fianeka.workorder.Model.Karyawan;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by galgadot on 29/01/18.
 */

public class Pengalaman {

    @SerializedName("id_peng")
    @Expose
    private String idpeng;

    @SerializedName("namainstitusi")
    @Expose
    private String namainstitusi;

    @SerializedName("jabatan")
    @Expose
    private String jabatan;

    @SerializedName("tahun")
    @Expose
    private String tahun;

    public String getIdpeng() {
        return idpeng;
    }

    public void setIdpeng(String idpeng) {
        this.idpeng = idpeng;
    }

    public String getNamainstitusi() {
        return namainstitusi;
    }

    public void setNamainstitusi(String namainstitusi) {
        this.namainstitusi = namainstitusi;
    }

    public String getJabatan() {
        return jabatan;
    }

    public void setJabatan(String jabatan) {
        this.jabatan = jabatan;
    }

    public String getTahun() {
        return tahun;
    }

    public void setTahun(String tahun) {
        this.tahun = tahun;
    }
}
