package me.fianeka.workorder.Model.Data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by galgadot on 28/01/18.
 */

public class KategoriBarang {

    @SerializedName("id_kat")
    @Expose
    private String idkat;

    @SerializedName("nama_kategori")
    @Expose
    private String kategori;

    @SerializedName("total")
    @Expose
    private String total;

    @SerializedName("photo")
    @Expose
    private String photo;

    public String getIdkat() {
        return idkat;
    }

    public void setIdkat(String idkat) {
        this.idkat = idkat;
    }

    public String getKategori() {
        return kategori;
    }

    public void setKategori(String kategori) {
        this.kategori = kategori;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }
}
