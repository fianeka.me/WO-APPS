package me.fianeka.workorder.Model.Data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import me.fianeka.workorder.Model.Karyawan.Karyawan;

/**
 * Created by galgadot on 30/01/18.
 */

public class JenisOrder {

    @SerializedName("id_jenisorder")
    @Expose
    private String idjenis;

    @SerializedName("id_subdiv")
    @Expose
    private String idsub;

    @SerializedName("namajenisorder")
    @Expose
    private String namajenis;

    @SerializedName("namasubdiv")
    @Expose
    private String namasubdiv;

    @SerializedName("action")
    @Expose
    private String action;

    @SerializedName("favicon")
    @Expose
    private String favicon;

    public String getIdjenis() {
        return idjenis;
    }

    public void setIdjenis(String idjenis) {
        this.idjenis = idjenis;
    }

    public String getIdsub() {
        return idsub;
    }

    public void setIdsub(String idsub) {
        this.idsub = idsub;
    }

    public String getNamajenis() {
        return namajenis;
    }

    public void setNamajenis(String namajenis) {
        this.namajenis = namajenis;
    }

    public String getNamasubdiv() {
        return namasubdiv;
    }

    public void setNamasubdiv(String namasubdiv) {
        this.namasubdiv = namasubdiv;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getFavicon() {
        return favicon;
    }

    public void setFavicon(String favicon) {
        this.favicon = favicon;
    }
}


