package me.fianeka.workorder.Model.Karyawan;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import me.fianeka.workorder.Model.Data.SubdivisiOrder;

/**
 * Created by galgadot on 29/01/18.
 */

public class ProfileData {

    @SerializedName("informasi")
    @Expose
    private List<Karyawan> informasi;

    @SerializedName("riwayat")
    @Expose
    private List<Riwayat> riwayat;

    @SerializedName("keluarga")
    @Expose
    private List<Keluarga> keluarga;

    @SerializedName("sertifikasi")
    @Expose
    private List<Kursus> sertifikasi;

    @SerializedName("pengalaman")
    @Expose
    private List<Pengalaman> pengalaman;

    public List<Karyawan> getInformasi() {
        return informasi;
    }

    public void setInformasi(List<Karyawan> informasi) {
        this.informasi = informasi;
    }

    public List<Riwayat> getRiwayat() {
        return riwayat;
    }

    public void setRiwayat(List<Riwayat> riwayat) {
        this.riwayat = riwayat;
    }

    public List<Keluarga> getKeluarga() {
        return keluarga;
    }

    public void setKeluarga(List<Keluarga> keluarga) {
        this.keluarga = keluarga;
    }

    public List<Kursus> getSertifikasi() {
        return sertifikasi;
    }

    public void setSertifikasi(List<Kursus> sertifikasi) {
        this.sertifikasi = sertifikasi;
    }

    public List<Pengalaman> getPengalaman() {
        return pengalaman;
    }

    public void setPengalaman(List<Pengalaman> pengalaman) {
        this.pengalaman = pengalaman;
    }
}
