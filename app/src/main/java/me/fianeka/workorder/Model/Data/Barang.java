package me.fianeka.workorder.Model.Data;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by galgadot on 01/02/18.
 */

public class Barang {
    @SerializedName("id_atk")
    @Expose
    private String idatk;

    @SerializedName("nama_barang")
    @Expose
    private String namabarang;

    @SerializedName("stok")
    @Expose
    private String stok;

    @SerializedName("id_kateg")
    @Expose
    private String idkategori;

    @SerializedName("nama_kategori")
    @Expose
    private String kategori;

    public String getIdatk() {
        return idatk;
    }

    public void setIdatk(String idatk) {
        this.idatk = idatk;
    }

    public String getNamabarang() {
        return namabarang;
    }

    public void setNamabarang(String namabarang) {
        this.namabarang = namabarang;
    }

    public String getStok() {
        return stok;
    }

    public void setStok(String stok) {
        this.stok = stok;
    }

    public String getIdkategori() {
        return idkategori;
    }

    public void setIdkategori(String idkategori) {
        this.idkategori = idkategori;
    }

    public String getKategori() {
        return kategori;
    }

    public void setKategori(String kategori) {
        this.kategori = kategori;
    }

    @Override
    public String toString() {
        return "Barang{" +
                "idatk='" + idatk + '\'' +
                ", namabarang='" + namabarang + '\'' +
                ", stok='" + stok + '\'' +
                ", idkategori='" + idkategori + '\'' +
                ", kategori='" + kategori + '\'' +
                '}';
    }
}
