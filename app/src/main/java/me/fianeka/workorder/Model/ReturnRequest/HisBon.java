package me.fianeka.workorder.Model.ReturnRequest;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import me.fianeka.workorder.Model.Data.HistoryBon;
import me.fianeka.workorder.Model.Data.HistoryOrder;

/**
 * Created by galgadot on 28/01/18.
 */

public class HisBon {

    @SerializedName("result")
    @Expose
    private Boolean result;

    @SerializedName("data")
    @Expose
    private List<HistoryBon> data;

    @SerializedName("message")
    @Expose
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getResult() {
        return result;
    }

    public void setResult(Boolean result) {
        this.result = result;
    }

    public List<HistoryBon> getData() {
        return data;
    }

    public void setData(List<HistoryBon> data) {
        this.data = data;
    }
}
