package me.fianeka.workorder.Fragment;


import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import me.fianeka.workorder.Adapter.TabAdapter;
import me.fianeka.workorder.Fragment.TabFragment.TabBon;
import me.fianeka.workorder.Fragment.TabFragment.TabOrder;
import me.fianeka.workorder.R;

/**
 * A simple {@link Fragment} subclass.
 */



public class HistoryFragment extends Fragment {
    public static String TAG = "HistoryFragment";

    private RelativeLayout rootview;
    private TabLayout tabLayout;
    private ViewPager viewPager;

    public HistoryFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        Log.d(TAG, "onCreateView: ");
        rootview = (RelativeLayout) inflater.inflate(R.layout.fragment_history, container, false);
        viewPager = (ViewPager) rootview.findViewById(R.id.pager);
        setupViewPager(viewPager);
        tabLayout = (TabLayout) rootview.findViewById(R.id.tabLayout);
        tabLayout.setupWithViewPager(viewPager);
        return rootview;
    }

    private void setupViewPager(ViewPager viewPager) {
        TabAdapter adapter = new TabAdapter(this.getChildFragmentManager());
        adapter.addFragment(new TabOrder(), "ASISTENSI ORDER");
        adapter.addFragment(new TabBon(), "BON ATK");
        viewPager.setAdapter(adapter);
    }

}
