package me.fianeka.workorder.Fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import me.fianeka.workorder.Adapter.KeluargaAdapter;
import me.fianeka.workorder.Adapter.KursusAdapter;
import me.fianeka.workorder.Adapter.PengalamanAdapter;
import me.fianeka.workorder.Adapter.RiwayatAdapter;
import me.fianeka.workorder.Helper.DBHelper;
import me.fianeka.workorder.Model.Karyawan.Karyawan;
import me.fianeka.workorder.Model.Karyawan.ProfileData;
import me.fianeka.workorder.Model.ReturnRequest.Profile;
import me.fianeka.workorder.R;
import me.fianeka.workorder.Services.Api;
import me.fianeka.workorder.Services.RegisterApi;
import me.fianeka.workorder.Services.RetroHelper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends Fragment {
    public static String TAG = "ProfileFragment";
    private RelativeLayout rootview;
    private DBHelper db;
    private RecyclerView list_keluaga,list_riwayat,list_kursus,list_peng;
    private LinearLayoutManager lkel,lriw,lkur,lpeng;
    private ProgressBar pgriwayat,pgkursus,pgpeng,pgkeluarga;
    private RiwayatAdapter adapter_riwayat;
    private KeluargaAdapter adapter_keluarga;
    private KursusAdapter adapter_kursus;
    private PengalamanAdapter adapter_pengalaman;


    public ProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView: ");
        rootview = (RelativeLayout) inflater.inflate(R.layout.fragment_profile, container, false);
        pgkeluarga = rootview.findViewById(R.id.pgkeluarga);
        pgriwayat = rootview.findViewById(R.id.pgriwayat);
        pgkursus = rootview.findViewById(R.id.pgkursus);
        pgpeng = rootview.findViewById(R.id.pgpeng);
        lkel = new LinearLayoutManager(getActivity());
        lkel.setOrientation(LinearLayoutManager.HORIZONTAL);
        list_keluaga= rootview.findViewById(R.id.list_keluarga);
        list_keluaga.setHasFixedSize(true);
        list_keluaga.setLayoutManager(lkel);

        lkur = new LinearLayoutManager(getActivity());
        lkur.setOrientation(LinearLayoutManager.HORIZONTAL);
        list_riwayat= rootview.findViewById(R.id.list_riwayat);
        list_riwayat.setHasFixedSize(true);
        list_riwayat.setLayoutManager(lkur);

        lriw = new LinearLayoutManager(getActivity());
        lriw.setOrientation(LinearLayoutManager.HORIZONTAL);
        list_kursus= rootview.findViewById(R.id.list_kursus);
        list_kursus.setHasFixedSize(true);
        list_kursus.setLayoutManager(lriw);

        lpeng = new LinearLayoutManager(getActivity());
        lpeng.setOrientation(LinearLayoutManager.HORIZONTAL);
        list_peng = rootview.findViewById(R.id.list_peng);
        list_peng.setHasFixedSize(true);
        list_peng.setLayoutManager(lpeng);
        db = new DBHelper(getActivity());
        getProfileData(db.getNik());
        return rootview;
    }

    private void getProfileData(final String nik) {
        RetroHelper help = new RetroHelper();
        RegisterApi res = help.getInstance().create(RegisterApi.class);
        Call<Profile> results = res.ProfileKaryawan(Api.API_KEY, nik);
        results.enqueue(new Callback<Profile>() {
            @Override
            public void onResponse(Call<Profile> call, Response<Profile> response) {
                final Profile myResponse = response.body();
                if (getActivity() != null) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            setProgreesbar(getActivity());
                            try {
                                Log.d("res", myResponse.getMessage());
                                if (myResponse.getResult()) {
                                    ProfileData temp = myResponse.getData();
                                    setInformasiPribadi(temp.getInformasi().get(0));
                                    //keluargalist
                                    adapter_riwayat = new RiwayatAdapter(getActivity(), temp.getRiwayat());
                                    list_riwayat.setAdapter(adapter_riwayat);

                                    adapter_keluarga = new KeluargaAdapter(getActivity(), temp.getKeluarga());
                                    list_keluaga.setAdapter(adapter_keluarga);

                                    adapter_kursus = new KursusAdapter(getActivity(), temp.getSertifikasi());
                                    list_kursus.setAdapter(adapter_kursus);

                                    adapter_pengalaman = new PengalamanAdapter(getActivity(), temp.getPengalaman());
                                    list_peng.setAdapter(adapter_pengalaman);

                                } else {
                                    String errorMsg = myResponse.getMessage();
                                    Toast.makeText(getActivity(), errorMsg, Toast.LENGTH_LONG).show();
                                }
                            }catch (Exception ex){
                                Toast.makeText(getActivity(), ex.getMessage(), Toast.LENGTH_LONG).show();
                            }
                        }
                    });
                }else{
                    getProfileData(nik);
                }

            }

            @Override
            public void onFailure(Call<Profile> call, Throwable t) {
                Log.d("error", t.getMessage());
            }
        });
    }

    private void setProgreesbar(FragmentActivity activity) {
        activity.runOnUiThread(() -> {
            pgkeluarga.setVisibility(View.GONE);
            pgkursus.setVisibility(View.GONE);
            pgpeng.setVisibility(View.GONE);
            pgriwayat.setVisibility(View.GONE);
        });
    }

    private void setInformasiPribadi(Karyawan karyawan) {
        ImageView profile;
        TextView namakar,nik;
        TextView jabatan,tgl_mulai,statuskar,alamat,telpon,ttl,jk,status,agama,kw,pdd;

        profile = rootview.findViewById(R.id.profile_image);
        namakar = rootview.findViewById(R.id.namakar);
        nik = rootview.findViewById(R.id.nik);
        jabatan = rootview.findViewById(R.id.jabatan);
        tgl_mulai = rootview.findViewById(R.id.tgl_mulai);
        statuskar = rootview.findViewById(R.id.statuskar);
        alamat = rootview.findViewById(R.id.alamat);
        telpon = rootview.findViewById(R.id.telpon);
        ttl = rootview.findViewById(R.id.ttl);
        status = rootview.findViewById(R.id.status);
        jk = rootview.findViewById(R.id.jk);
        agama = rootview.findViewById(R.id.agama);
        kw = rootview.findViewById(R.id.kw);
        pdd = rootview.findViewById(R.id.pdd);

        Glide.with(getActivity()).load(karyawan.getPhoto())
                .crossFade()
                .thumbnail(0.5f)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(profile);
        namakar.setText(karyawan.getNama());
        nik.setText(karyawan.getNik());
        jabatan.setText(karyawan.getJabatan());
        tgl_mulai.setText(karyawan.getTgl_mulaikerja());
        statuskar.setText(karyawan.getStatuskaryawan());
        alamat.setText(karyawan.getAlamat());
        telpon.setText(karyawan.getNotel());
        ttl.setText(karyawan.getTempat_lahir()+", "+karyawan.getTgl_lahir());
        status.setText(karyawan.getStatus());
        String jeniskel = karyawan.getJekel().equals("L") ? "Laki-Laki" : "Perempuan";
        jk.setText(jeniskel);
        agama.setText(karyawan.getAgama());
        kw.setText(karyawan.getKewarganegaraan());
        pdd.setText(karyawan.getPendidikan());
    }

}
