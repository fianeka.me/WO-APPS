package me.fianeka.workorder.Fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import me.fianeka.workorder.Activity.BarangActivity;
import me.fianeka.workorder.Activity.JenisOrder;
import me.fianeka.workorder.Adapter.KategoriBarangAdapter;
import me.fianeka.workorder.Adapter.SubdivOrderAdapter;
import me.fianeka.workorder.Model.Data.KategoriBarang;
import me.fianeka.workorder.Model.Data.SubdivisiOrder;
import me.fianeka.workorder.Model.ReturnRequest.KatBarang;
import me.fianeka.workorder.Model.ReturnRequest.Subdivorder;
import me.fianeka.workorder.R;
import me.fianeka.workorder.Services.Api;
import me.fianeka.workorder.Services.RegisterApi;
import me.fianeka.workorder.Services.RetroHelper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment implements View.OnClickListener {
    public static String TAG = "HomeFragment";
    private RelativeLayout rootview;
    private RecyclerView list_subdviv,list_kategori;
    private LinearLayoutManager llm, llm1;
    private ProgressBar pgsubdiv, pgkategori;
    private List<SubdivisiOrder> datas;
    private List<KategoriBarang> datas1;
    private SubdivOrderAdapter adapter_subdiv;
    private KategoriBarangAdapter adapter_kategori;
    private TextView allbarang,alljenis;



    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        Log.d(TAG, "onCreateView: ");
        rootview = (RelativeLayout) inflater.inflate(R.layout.fragment_home, container, false);
        pgsubdiv = rootview.findViewById(R.id.pgsubdiv);
        pgkategori = rootview.findViewById(R.id.pgkategori);
        llm = new LinearLayoutManager(getActivity());
        llm.setOrientation(LinearLayoutManager.HORIZONTAL);
        llm1 = new LinearLayoutManager(getActivity());
        llm1.setOrientation(LinearLayoutManager.HORIZONTAL);
        list_kategori = rootview.findViewById(R.id.list_kategori);
        list_kategori.setHasFixedSize(true);
        list_kategori.setLayoutManager(llm);
        list_subdviv = rootview.findViewById(R.id.list_subdiv);
        list_subdviv.setHasFixedSize(true);
        list_subdviv.setLayoutManager(llm1);
        allbarang = rootview.findViewById(R.id.allbarang);
        alljenis= rootview.findViewById(R.id.alljenis);
        alljenis.setOnClickListener(this);
        allbarang.setOnClickListener(this);
        getSubdivOrder();
        return rootview;
    }

    private void getSubdivOrder() {
        RetroHelper help = new RetroHelper();
        RegisterApi res = help.getInstance().create(RegisterApi.class);
        Call<Subdivorder> results = res.SubdivOrder(Api.API_KEY);
        results.enqueue(new Callback<Subdivorder>() {
            @Override
            public void onResponse(Call<Subdivorder> call, Response<Subdivorder> response) {
                final Subdivorder myResponse = response.body();
                if (getActivity() != null) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                Log.d("res", myResponse.getMessage());
                                if (myResponse.getResult()) {
                                    List<SubdivisiOrder> temp = myResponse.getData();
                                    if (temp.size() <= 0) {
                                        pgsubdiv.setVisibility(View.GONE);
                                    } else {
                                        HomeFragment.this.datas = temp;
                                        adapter_subdiv = new SubdivOrderAdapter(getActivity(), datas);
                                        list_subdviv.setAdapter(adapter_subdiv);
                                        pgsubdiv.setVisibility(View.GONE);
                                        getKategoriAtk();
                                    }
                                } else {
                                    String errorMsg = myResponse.getMessage();
                                    Toast.makeText(getActivity(), errorMsg, Toast.LENGTH_LONG).show();
                                }
                            }catch (Exception ex){
                                Toast.makeText(getActivity(), ex.getMessage(), Toast.LENGTH_LONG).show();
                            }
                        }
                    });
                }else{
                    getSubdivOrder();
                }

            }

            @Override
            public void onFailure(Call<Subdivorder> call, Throwable t) {
                Log.d("error", t.getMessage());
            }
        });
    }

    private void getKategoriAtk() {
        RetroHelper help = new RetroHelper();
        RegisterApi res = help.getInstance().create(RegisterApi.class);
        Call<KatBarang> results = res.KategoriAtk(Api.API_KEY);
        results.enqueue(new Callback<KatBarang>() {
            @Override
            public void onResponse(Call<KatBarang> call, Response<KatBarang> response) {
                final KatBarang myResponse = response.body();
                if (getActivity() != null) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                Log.d("res", myResponse.getMessage());
                                if (myResponse.getResult()) {
                                    List<KategoriBarang> temp = myResponse.getData();
                                    if (temp.size() <= 0) {
                                        pgkategori.setVisibility(View.GONE);
                                    } else {
                                        HomeFragment.this.datas1 = temp;
                                        adapter_kategori = new KategoriBarangAdapter(getActivity(), datas1);
                                        list_kategori.setAdapter(adapter_kategori);
//                                        list_kategori.setAdapter(adapter_subdiv);
//                                        adapter_products.setKlikProduct(PromoTab.this);
                                        pgkategori.setVisibility(View.GONE);

                                    }
                                } else {
                                    String errorMsg = myResponse.getMessage();
                                    Toast.makeText(getActivity(), errorMsg, Toast.LENGTH_LONG).show();
                                }
                            }catch (Exception ex){
                                Toast.makeText(getActivity(), ex.getMessage(), Toast.LENGTH_LONG).show();
                            }
                        }
                    });
                }else{
                    getSubdivOrder();
                }

            }

            @Override
            public void onFailure(Call<KatBarang> call, Throwable t) {
                Log.d("error", t.getMessage());
            }
        });
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == alljenis.getId()) {
            Intent mainIntent = new Intent(getActivity(),JenisOrder.class);
            mainIntent.putExtra("idsub","0");
            startActivity(mainIntent);
        }
        if (v.getId() == allbarang.getId()) {
            Intent mainIntent = new Intent(getActivity(),BarangActivity.class);
            mainIntent.putExtra("idkat","0");
            startActivity(mainIntent);
        }
    }
}
