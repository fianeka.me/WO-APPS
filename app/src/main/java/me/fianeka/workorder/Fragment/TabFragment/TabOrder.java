package me.fianeka.workorder.Fragment.TabFragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import me.fianeka.workorder.Adapter.HistoryOrderAdapter;
import me.fianeka.workorder.Helper.DBHelper;
import me.fianeka.workorder.Model.ReturnRequest.HisOrder;
import me.fianeka.workorder.R;
import me.fianeka.workorder.Services.Api;
import me.fianeka.workorder.Services.RegisterApi;
import me.fianeka.workorder.Services.RetroHelper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class TabOrder extends Fragment {
    public static String TAG = "TabOrder";

    private RelativeLayout rootview;
    private DBHelper db;
    private RecyclerView list_item;
    private LinearLayoutManager ll;
    private ProgressBar pgitem;
    private HistoryOrderAdapter h_adapter;
    public TabOrder() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        Log.d(TAG, "onCreateView: ");
        rootview = (RelativeLayout) inflater.inflate(R.layout.fragment_taborder, container, false);
        db = new DBHelper(getActivity());
        pgitem = rootview.findViewById(R.id.pgitem);
        ll = new LinearLayoutManager(getActivity());
        ll.setOrientation(LinearLayoutManager.VERTICAL);
        list_item = rootview.findViewById(R.id.list_item);
        list_item.setHasFixedSize(true);
        list_item.setLayoutManager(ll);
        getHistory(db.getNik());
        return rootview;
    }

    private void getHistory(final String nik) {
        RetroHelper help = new RetroHelper();
        RegisterApi res = help.getInstance().create(RegisterApi.class);
        Call<HisOrder> results = res.HistoryOrder(Api.API_KEY, nik);
        results.enqueue(new Callback<HisOrder>() {
            @Override
            public void onResponse(Call<HisOrder> call, Response<HisOrder> response) {
                final HisOrder myResponse = response.body();
                if (getActivity() != null) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            pgitem.setVisibility(View.GONE);
                            try {
                                Log.d("res", myResponse.getMessage());
                                if (myResponse.getResult()) {
//                                    ProfileData temp = myResponse.getData();
                                    //keluargalist
                                    h_adapter = new HistoryOrderAdapter(getActivity(), myResponse.getData());
                                    list_item.setAdapter(h_adapter);
                                } else {
                                    String errorMsg = myResponse.getMessage();
                                    Toast.makeText(getActivity(), errorMsg, Toast.LENGTH_LONG).show();
                                }
                            }catch (Exception ex){
                                Toast.makeText(getActivity(), ex.getMessage(), Toast.LENGTH_LONG).show();
                            }
                        }
                    });
                }else{
                    getHistory(nik);
                }

            }

            @Override
            public void onFailure(Call<HisOrder> call, Throwable t) {
                Log.d("error", t.getMessage());
            }
        });
    }

}
