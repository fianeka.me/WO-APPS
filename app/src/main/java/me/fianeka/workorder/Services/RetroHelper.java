package me.fianeka.workorder.Services;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import me.fianeka.workorder.Services.Api;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by galgadot on 01/11/17.
 */

public class RetroHelper {

    public Retrofit getInstance(){
        Gson gson = new GsonBuilder().setLenient().create();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Api.ROOT_URL).addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        return retrofit;
    }
}
