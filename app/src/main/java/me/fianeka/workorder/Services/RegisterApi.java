package me.fianeka.workorder.Services;


import me.fianeka.workorder.Model.Data.KategoriBarang;
import me.fianeka.workorder.Model.ReturnRequest.HisBon;
import me.fianeka.workorder.Model.ReturnRequest.HisOrder;
import me.fianeka.workorder.Model.ReturnRequest.Insert;
import me.fianeka.workorder.Model.ReturnRequest.JenBarang;
import me.fianeka.workorder.Model.ReturnRequest.JenOrder;
import me.fianeka.workorder.Model.ReturnRequest.KatBarang;
import me.fianeka.workorder.Model.ReturnRequest.Login;
import me.fianeka.workorder.Model.ReturnRequest.Profile;
import me.fianeka.workorder.Model.ReturnRequest.Subdivorder;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by galgadot on 01/11/17.
 */

public interface RegisterApi {

    @FormUrlEncoded
    @POST("login")
    Call<Login> UserLogin(
            @Field("key") String key,
            @Field("nik") String username,
            @Field("password") String pass);

    @FormUrlEncoded
    @POST("subdiv")
    Call<Subdivorder> SubdivOrder(
            @Field("key") String key);

    @FormUrlEncoded
    @POST("kategori")
    Call<KatBarang> KategoriAtk(
            @Field("key") String key);

    @FormUrlEncoded
    @POST("profile")
    Call<Profile> ProfileKaryawan(
            @Field("key") String key,
            @Field("nik") String nik);

    @FormUrlEncoded
    @POST("historyorder")
    Call<HisOrder> HistoryOrder(
            @Field("key") String key,
            @Field("nik") String nik);

    @FormUrlEncoded
    @POST("orderdetail")
    Call<HisOrder> OrderDetail(
            @Field("key") String key,
            @Field("nik") String nik,
            @Field("idorder") String idorder);

    @FormUrlEncoded
    @POST("historybon")
    Call<HisBon> HistoryBon(
            @Field("key") String key,
            @Field("nik") String nik);

    @FormUrlEncoded
    @POST("jenisorder")
    Call<JenOrder> JenisOrder(
            @Field("key") String key,
            @Field("idsub") String idsub);

    @FormUrlEncoded
    @POST("barangatk")
    Call<JenBarang> BarangAtk(
            @Field("key") String key,
            @Field("idkategori") String idkategori);

    @FormUrlEncoded
    @POST("password")
    Call<Insert> ChangePassword(
            @Field("key") String key,
            @Field("nik") String nik,
            @Field("old_password") String old_password,
            @Field("new_password") String new_password,
            @Field("confirm_password") String confirm_password);

    @FormUrlEncoded
    @POST("bon")
    Call<Insert> Bon(
            @Field("key") String key,
            @Field("nik") String nik,
            @Field("jumlah") String jumlah,
            @Field("idatk") String idatk);


    @FormUrlEncoded
    @POST("order")
    Call<Insert> Order(
            @Field("key") String key,
            @Field("nik") String nik,
            @Field("idjenis") String idjenis,
            @Field("keterangan") String keterangan,
            @Field("idsub") String idsub);

//    @FormUrlEncoded
//    @POST(Api.URL_GETMAIN)
//    Call<uKeuReturn> getMain(
//            @Field("nis") String nis,
//            @Field("thn") int thn);
//
//    @FormUrlEncoded
//    @POST(Api.URL_GETDETAIL)
//    Call<DetailReturn> getDetail(
//            @Field("nis") String nis,
//            @Field("thn") int thn);
//
//    @FormUrlEncoded
//    @POST(Api.URL_GETTRANSFERS)
//    Call<TransfersReturn> getTransfers(
//            @Field("nis") String nis);
//
//    @FormUrlEncoded
//    @POST(Api.URL_KEUTAHUNAN)
//    Call<MKTReturn> getKeuTahunan(
//            @Field("thn") int thn);
//
//    @FormUrlEncoded
//    @POST(Api.URL_KEUBULANAN)
//    Call<MBulanReturn> getKeuBulanan(
//            @Field("thn") int thn);
//
//
//    @GET(Api.URL_GETTAHUNAJARAN)
//    Call<TAjaranReturn> getTajaran();
//
//    @GET(Api.URL_GETIMAGEBANNER)
//    Call<ImageReturn> getBanner();


}
