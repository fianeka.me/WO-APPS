package me.fianeka.workorder.Activity;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import me.fianeka.workorder.Helper.DBHelper;
import me.fianeka.workorder.Model.ReturnRequest.Insert;
import me.fianeka.workorder.R;
import me.fianeka.workorder.Services.Api;
import me.fianeka.workorder.Services.RegisterApi;
import me.fianeka.workorder.Services.RetroHelper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Password extends AppCompatActivity {
    private Toolbar tbar;
    private DBHelper db;
    private EditText passlama,passbaru,passbaruc;
    private Button btn_order;
    private Snackbar snackbar;
    private CoordinatorLayout root;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password);
        tbar = findViewById(R.id.toolbar);
        db = new DBHelper(this);
        set_action_bar();
        root = findViewById(R.id.rootlayout);
        passlama = findViewById(R.id.passlama);
        passbaru = findViewById(R.id.passbaru);
        passbaruc = findViewById(R.id.passbaruc);
        btn_order = findViewById(R.id.btn_order);
        btn_order.setOnClickListener(v -> {
            if (validateForm()) {
                changePassword(passlama.getText().toString(),passbaru.getText().toString(),passbaruc.getText().toString());
            }
        });
    }
///bagian ini belum cek indexnya dan pabagina dibagian netrol button untu control kesaman, validai miiman 6
    private void changePassword(String old, String newpas, String confirm) {
        Log.d("This", old+" "+newpas+" "+confirm);
        RetroHelper help = new RetroHelper();
        RegisterApi res = help.getInstance().create(RegisterApi.class);
        Call<Insert> results = res.ChangePassword(Api.API_KEY, db.getNik(), old, newpas, confirm);
        results.enqueue(new Callback<Insert>() {
            @Override
            public void onResponse(Call<Insert> call, Response<Insert> response) {
                final Insert myResponse = response.body();
                runOnUiThread(() -> {
                    try {
                        Log.d("res", myResponse.getMessage());
                        if (myResponse.getResult()) {
                            AlertDialog alertDialog = new AlertDialog.Builder(Password.this).create();
                            alertDialog.setTitle("Berhasil");
                            alertDialog.setMessage(myResponse.getMessage());
                            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Close",
                                    (dialog, which) -> {
                                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        startActivity(intent);
                                        dialog.dismiss();
                                    });
                            alertDialog.setCancelable(false);
                            alertDialog.show();
                        } else {
                            show_snackbar(myResponse.getMessage());
                        }
                    } catch (Exception ex) {
                        Log.d("Eror", ex.getMessage());
                        show_snackbar(ex.getMessage());
                    }
                });
            }

            @Override
            public void onFailure(Call<Insert> call, Throwable t) {
                show_snackbar(t.getMessage());

            }
        });
    }

    private boolean validateForm() {
        String pl = passlama.getText().toString().trim();
        String pb = passbaru.getText().toString().trim();
        String pbc = passbaruc.getText().toString().trim();
        if (pl.isEmpty() || pb.isEmpty() || pbc.isEmpty()) {
            show_snackbar("Seluruh inputan diperlukan");
            return false;
        }
        if (pl.length()<6 || pb.length()<6 || pbc.length()<6) {
            show_snackbar("Panjang password harus lebih besar dari enam");
            return false;
        }
        return true;
    }

    private void show_snackbar(String pesan) {
        snackbar = Snackbar
                .make(root, pesan, Snackbar.LENGTH_INDEFINITE)
                .setAction("Close", view -> snackbar.dismiss());
        snackbar.setActionTextColor(ContextCompat.getColor(snackbar.getContext(), R.color.colorActiveSmall));
        View snackbarView = snackbar.getView();
        snackbarView.setBackgroundColor(Color.parseColor("#996b3a"));
        TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextSize(12);
        textView.setTextColor(Color.WHITE);
        snackbar.show();
    }

    private void set_action_bar() {
        setSupportActionBar(tbar);
        ActionBar ab = getSupportActionBar();
        getSupportActionBar().setTitle("Ganti Password");
        ab.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#996b3a"))); //without theme);
        TextView tv = new TextView(getApplicationContext());
        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT);
        tv.setLayoutParams(lp);
        tv.setText(ab.getTitle());
        tv.setTextColor(Color.WHITE);
        tv.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 16);
        ab.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        ab.setCustomView(tv);
        ab.setDisplayHomeAsUpEnabled(true);
        ab.setHomeAsUpIndicator(R.drawable.ic_back);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
