package me.fianeka.workorder.Activity.OrderForm;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.text.format.DateFormat;
import android.util.Log;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.firebase.messaging.FirebaseMessaging;
import com.takisoft.datetimepicker.TimePickerDialog;

import java.util.Calendar;

import me.fianeka.workorder.Activity.MainActivity;
import me.fianeka.workorder.Helper.DBHelper;
import me.fianeka.workorder.Model.ReturnRequest.Insert;
import me.fianeka.workorder.R;
import me.fianeka.workorder.Services.Api;
import me.fianeka.workorder.Services.RegisterApi;
import me.fianeka.workorder.Services.RetroHelper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FormKendaraan extends AppCompatActivity implements View.OnClickListener {
    private Toolbar tbar;
    private DBHelper db;
    private EditText acara,jampergi,kode, keterangan;
    private Button btn_order;
    private Snackbar snackbar;
    private ConstraintLayout root;
    private Calendar cal = Calendar.getInstance();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_kendaraan);
        root = findViewById(R.id.rootlayout);
        tbar = findViewById(R.id.toolbar);
        db = new DBHelper(this);
        set_action_bar();
        acara = findViewById(R.id.acara);
        jampergi = findViewById(R.id.jampergi);
        kode = findViewById(R.id.kode);
        keterangan = findViewById(R.id.keterangan);
        btn_order = findViewById(R.id.btn_order);

        jampergi.setInputType(InputType.TYPE_NULL);
        jampergi.setOnClickListener(this);
        btn_order.setOnClickListener(v -> {
            if (validateForm()) {
                storeDataOrder(acara.getText().toString(),jampergi.getText().toString(), kode.getText().toString(), keterangan.getText().toString());
            }
        });
    }

    private void storeDataOrder(String acara, String jam, String kode, String keterangan) {
        String idsub = getIntent().getStringExtra("idsub");
        String idjen = getIntent().getStringExtra("idjenis");
        String ket = "Pesanan Kendaraan Dinas Untuk Acara/Keperluan: "+acara+", Jam Keberangkatan: "+jam+", Dengan Kode Proyek: "+kode+" Keterangan Tambahan: "+keterangan;
        Log.d("data", idsub + " " + idjen + " " + ket);
        RetroHelper help = new RetroHelper();
        RegisterApi res = help.getInstance().create(RegisterApi.class);
        Call<Insert> results = res.Order(Api.API_KEY, db.getNik(), idjen, ket, idsub);
        results.enqueue(new Callback<Insert>() {
            @Override
            public void onResponse(Call<Insert> call, Response<Insert> response) {
                final Insert myResponse = response.body();
                runOnUiThread(() -> {
                    try {
                        Log.d("res", myResponse.getMessage());
                        if (myResponse.getResult()) {
//                            String string = "004-034556";
                            String[] parts = myResponse.getMessage().split("-");
                            Log.d("Subscribe To Firebase", "order"+parts[0]);
                            FirebaseMessaging.getInstance().subscribeToTopic("order"+parts[0]);
                            AlertDialog alertDialog = new AlertDialog.Builder(FormKendaraan.this).create();
                            alertDialog.setTitle("Selamat !!");
                            alertDialog.setMessage("Order sedang diproses, jika diperlukan silahkan hubungi SDM. Terimkasih.");
                            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Close",
                                    (dialog, which) -> {
                                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        startActivity(intent);
                                        dialog.dismiss();
                                    });
                            alertDialog.setCancelable(false);
                            alertDialog.show();
                        } else {
                            show_snackbar(myResponse.getMessage());
                        }
                    } catch (Exception ex) {
                        Log.d("Eror", ex.getMessage());
                        show_snackbar(ex.getMessage());
                    }
                });
            }

            @Override
            public void onFailure(Call<Insert> call, Throwable t) {
                show_snackbar(t.getMessage());

            }
        });
    }

    private boolean validateForm() {
        String kd = kode.getText().toString().trim();
        String acaraval = acara.getText().toString().trim();
        String jamp = jampergi.getText().toString().trim();
        String ktr = keterangan.getText().toString().trim();
        if (acaraval.isEmpty()) {
            show_snackbar("Nama keperluan / acara diperlukan");
            return false;
        }
        if (jamp.isEmpty()) {
            show_snackbar("Infokan jam pergi kendaraan");
            return false;
        }
        if (kd.isEmpty()) {
            show_snackbar("Infokan kode proyek orderan");
            return false;
        }
        if (ktr.isEmpty()) {
            show_snackbar("Infokan kami detail pesanan anda");
            return false;
        }
        return true;

    }

    private void show_snackbar(String pesan) {
        snackbar = Snackbar
                .make(root, pesan, Snackbar.LENGTH_INDEFINITE)
                .setAction("Close", view -> snackbar.dismiss());
        snackbar.setActionTextColor(ContextCompat.getColor(snackbar.getContext(), R.color.colorActiveSmall));
        View snackbarView = snackbar.getView();
        snackbarView.setBackgroundColor(Color.parseColor("#996b3a"));
        TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextSize(12);
        textView.setTextColor(Color.WHITE);
        snackbar.show();
    }

    private void showTime(final EditText datetext) {
        TimePickerDialog tpd = new TimePickerDialog(this, (view1, hourOfDay, minute) -> {
            datetext.setText(String.format("%02d", hourOfDay) + ":" + String.format("%02d", minute));
        }, cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), DateFormat.is24HourFormat(this));
        tpd.show();
    }

    private void set_action_bar() {
        setSupportActionBar(tbar);
        ActionBar ab = getSupportActionBar();
        getSupportActionBar().setTitle("Order Kendaraan");
        ab.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#996b3a"))); //without theme);
        TextView tv = new TextView(getApplicationContext());
        // Create a LayoutParams for TextView
        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT, // Width of TextView
                RelativeLayout.LayoutParams.WRAP_CONTENT); // Height of TextView

        // Apply the layout parameters to TextView widget
        tv.setLayoutParams(lp);

        // Set text to display in TextView
        tv.setText(ab.getTitle()); // ActionBar title text

        // Set the text color of TextView to black
        // This line change the ActionBar title text color
        tv.setTextColor(Color.WHITE);
        // Set the TextView text size in dp
        // This will change the ActionBar title text size
        tv.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 16);

        // Set the ActionBar display option
        ab.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);

        // Finally, set the newly created TextView as ActionBar custom view
        ab.setCustomView(tv);
        ab.setDisplayHomeAsUpEnabled(true);
        ab.setHomeAsUpIndicator(R.drawable.ic_back);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id==android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == jampergi.getId()) {
            showTime(jampergi);
        }
    }
}
