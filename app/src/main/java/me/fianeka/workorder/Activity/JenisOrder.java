package me.fianeka.workorder.Activity;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.MenuItem;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import me.fianeka.workorder.Activity.OrderForm.FormKendaraan;
import me.fianeka.workorder.Activity.OrderForm.FormMakan;
import me.fianeka.workorder.Activity.OrderForm.FormMinum;
import me.fianeka.workorder.Activity.OrderForm.FormPengadaan;
import me.fianeka.workorder.Activity.OrderForm.FormPerbaikan;
import me.fianeka.workorder.Activity.OrderForm.FormTiket;
import me.fianeka.workorder.Adapter.JenisOrderAdapter;
import me.fianeka.workorder.Model.ReturnRequest.JenOrder;
import me.fianeka.workorder.R;
import me.fianeka.workorder.Services.Api;
import me.fianeka.workorder.Services.RegisterApi;
import me.fianeka.workorder.Services.RetroHelper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class JenisOrder extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener, JenisOrderAdapter.KlikOrder {
    private Toolbar tbar;
    private SwipeRefreshLayout swiper;
    private String idsub;
    private RecyclerView list_item;
    private LinearLayoutManager ll;
    private JenisOrderAdapter h_adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jenis_order);
        tbar = findViewById(R.id.toolbar);
        idsub = getIntent().getStringExtra("idsub");
        set_action_bar();
        ll = new LinearLayoutManager(this);
        ll.setOrientation(LinearLayoutManager.VERTICAL);
        list_item = findViewById(R.id.list_item);
        list_item.setHasFixedSize(true);
        list_item.setLayoutManager(ll);
        swiper = findViewById(R.id.swiper);
        swiper.setOnRefreshListener(this);
        swiper.setColorSchemeResources(
                R.color.colorPrimary,
                R.color.colorAccent,
                R.color.colorPrimary);
        swiper.post(new Runnable() {
                        @Override
                        public void run() {
                            swiper.setRefreshing(true);
                            getJenisOrder(idsub);
                        }
                    }
        );
    }

    private void getJenisOrder(String idsub) {
        RetroHelper help = new RetroHelper();
        RegisterApi res = help.getInstance().create(RegisterApi.class);
        Call<JenOrder> results = res.JenisOrder(Api.API_KEY, idsub);
        results.enqueue(new Callback<JenOrder>() {
            @Override
            public void onResponse(Call<JenOrder> call, Response<JenOrder> response) {
                final JenOrder myResponse = response.body();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        swiper.setRefreshing(false);
                        try {
                            Log.d("res", myResponse.getMessage());
                            if (myResponse.getResult()) {
                                h_adapter = new JenisOrderAdapter(JenisOrder.this, myResponse.getData());
                                h_adapter.setKlikOrder(JenisOrder.this);
                                list_item.setAdapter(h_adapter);

                            } else {
                                String errorMsg = myResponse.getMessage();
                                Toast.makeText(JenisOrder.this, errorMsg, Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception ex) {
                            Toast.makeText(JenisOrder.this, ex.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }
                });
            }

            @Override
            public void onFailure(Call<JenOrder> call, Throwable t) {
//                Log.d("error", t.getMessage());
            }
        });
    }

    @Override
    public void onRefresh() {
        getJenisOrder(idsub);
    }

    private void set_action_bar() {
        setSupportActionBar(tbar);
        ActionBar ab = getSupportActionBar();
        if (getIntent().hasExtra("title")) {
            getSupportActionBar().setTitle(getIntent().getStringExtra("title"));
        } else {
            getSupportActionBar().setTitle("Semua Sudivisi");
        }
        ab.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#996b3a"))); //without theme);
        TextView tv = new TextView(getApplicationContext());
        // Create a LayoutParams for TextView
        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT, // Width of TextView
                RelativeLayout.LayoutParams.WRAP_CONTENT); // Height of TextView

        // Apply the layout parameters to TextView widget
        tv.setLayoutParams(lp);

        // Set text to display in TextView
        tv.setText(ab.getTitle()); // ActionBar title text

        // Set the text color of TextView to black
        // This line change the ActionBar title text color
        tv.setTextColor(Color.WHITE);
        // Set the TextView text size in dp
        // This will change the ActionBar title text size
        tv.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 16);

        // Set the ActionBar display option
        ab.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);

        // Finally, set the newly created TextView as ActionBar custom view
        ab.setCustomView(tv);
        ab.setDisplayHomeAsUpEnabled(true);
        ab.setHomeAsUpIndicator(R.drawable.ic_back);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public void onKlikOrder(me.fianeka.workorder.Model.Data.JenisOrder selected) {
        Intent mainIntent;
        if (selected.getAction().equals("makan")) {
            mainIntent = new Intent(this, FormMakan.class);
        } else if (selected.getAction().equals("minum")) {
            mainIntent = new Intent(this, FormMinum.class);
        } else if (selected.getAction().equals("tiket")) {
            mainIntent = new Intent(this, FormTiket.class);
        } else if (selected.getAction().equals("kendaraan")) {
            mainIntent = new Intent(this, FormKendaraan.class);
        } else if (selected.getAction().equals("perbaikan")) {
            mainIntent = new Intent(this, FormPerbaikan.class);
        } else if (selected.getAction().equals("pengadaan")) {
            mainIntent = new Intent(this, FormPengadaan.class);
        } else {
            Toast.makeText(this, "Belum Bisa Melakukan Orderan", Toast.LENGTH_SHORT).show();
            mainIntent = null;
        }
        if (mainIntent != null) {
            mainIntent.putExtra("idsub", selected.getIdsub());
            mainIntent.putExtra("idjenis", selected.getIdjenis());
            startActivity(mainIntent);
        }

    }
}
