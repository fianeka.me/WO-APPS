package me.fianeka.workorder.Activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationAdapter;
import com.google.firebase.messaging.FirebaseMessaging;

import me.fianeka.workorder.Fragment.HistoryFragment;
import me.fianeka.workorder.Fragment.HomeFragment;
import me.fianeka.workorder.Fragment.ProfileFragment;
import me.fianeka.workorder.Helper.DBHelper;
import me.fianeka.workorder.Helper.SharedPreferenceHelper;
import me.fianeka.workorder.R;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private AHBottomNavigationAdapter navigationAdapter;
    private int[] tabColors;
    private Toolbar tbar;
    private ImageView logo, setting, logout;
    private TextView title;
    private DBHelper db;
    private SharedPreferenceHelper sp;
    private AHBottomNavigation bottomNavigation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        db = new DBHelper(getApplicationContext());
        sp = new SharedPreferenceHelper(getApplicationContext());
        Log.d("AfterLogin", db.getNik());
        if (getIntent().getStringExtra("message")!=null){
            Log.d("messagefire", getIntent().getStringExtra("message"));
        }
        initUI();
    }

    private void initUI() {
        tbar = findViewById(R.id.toolbar);

        setCustomBar();
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        }

        bottomNavigation = findViewById(R.id.bottom_navigation);
        tabColors = getApplicationContext().getResources().getIntArray(R.array.tab_colors);
        navigationAdapter = new AHBottomNavigationAdapter(this, R.menu.bottom_navigation_menu_3);
        navigationAdapter.setupWithBottomNavigation(bottomNavigation, tabColors);
        bottomNavigation.setAccentColor(Color.parseColor("#996b3a"));
//        bottomNavigation.setInactiveColor(Color.parseColor("#edae77"));
        bottomNavigation.setTranslucentNavigationEnabled(true);
        bottomNavigation.setOnTabSelectedListener(new AHBottomNavigation.OnTabSelectedListener() {
            @Override
            public boolean onTabSelected(int position, boolean wasSelected) {
                Fragment selectedFragment = null;
                if (position == 0) {
                    selectedFragment = new HomeFragment();
                    title.setText("Working Order");
                } else if (position == 1) {
                    selectedFragment = new ProfileFragment();
                    title.setText("Profile Anda");

                } else if (position == 2) {
                    title.setText("History");
                    selectedFragment = new HistoryFragment();
                }
                if (selectedFragment != null) {
                    FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
                    ft.replace(R.id.content_layout, selectedFragment);
                    ft.detach(selectedFragment).attach(selectedFragment).commit();
                }
                return true;
            }
        });
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.content_layout, new HomeFragment());
        transaction.commit();
    }

    public void setCustomBar() {
        setSupportActionBar(tbar);
        ActionBar ab = getSupportActionBar();
        ab.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        final View tes = LayoutInflater.from(MainActivity.this).inflate(R.layout.custom_toolbar, null, false);
        title = tes.findViewById(R.id.txt_titel);
        logo = tes.findViewById(R.id.logo);
        setting = tes.findViewById(R.id.setting);
        logout = tes.findViewById(R.id.logout);
        setting.setOnClickListener(this);
        logout.setOnClickListener(this);
        ab.setCustomView(tes);
        ab.setDisplayHomeAsUpEnabled(false);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == logout.getId()) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Logout")
                    .setMessage("Klik tombol ok untuk keluar dari aplikasi. Terimakasih :)")
                    .setPositiveButton("Keluar", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            sp.setLogin(false);
                            db.deleteUsers();
                            Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                            startActivity(intent);
                            finish();
                        }
                    })
                    .setNegativeButton("Kembali", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .show();

        }
        if (v.getId() == setting.getId()){
            Intent intent = new Intent(MainActivity.this, Password.class);
            startActivity(intent);
        }

    }
}
