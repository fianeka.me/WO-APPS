package me.fianeka.workorder.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;

import me.fianeka.workorder.Helper.DBHelper;
import me.fianeka.workorder.Helper.SharedPreferenceHelper;
import me.fianeka.workorder.Model.Karyawan.Karyawan;
import me.fianeka.workorder.Model.ReturnRequest.Login;
import me.fianeka.workorder.R;
import me.fianeka.workorder.Services.Api;
import me.fianeka.workorder.Services.RegisterApi;
import me.fianeka.workorder.Services.RetroHelper;
import mehdi.sakout.fancybuttons.FancyButton;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {
    private SharedPreferenceHelper sp;
    private DBHelper db;
    private EditText username,password;
    private FancyButton btnlogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_login);
        username = findViewById(R.id.username);
        password = findViewById(R.id.password);
        btnlogin = findViewById(R.id.btnfb);
        db = new DBHelper(getApplicationContext());
        sp = new SharedPreferenceHelper(getApplicationContext());
        if (sp.isLoggedIn()) {
            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
            startActivity(intent);
            finish();
        }
        btnlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String usn = username.getText().toString().trim();
                String psd = password.getText().toString().trim();
                if (!usn.isEmpty() && !psd.isEmpty()) {
                    LoginProses(usn, psd);
                } else {
                    Toast.makeText(getApplicationContext(),
                            "Username Dan Password Tidak Boleh Kosong", Toast.LENGTH_LONG)
                            .show();
                }
            }
        });
    }

    private void LoginProses(String usn, String psd) {
        RetroHelper help = new RetroHelper();
        RegisterApi res = help.getInstance().create(RegisterApi.class);
        Call<Login> results = res.UserLogin(Api.API_KEY,usn,psd);
        results.enqueue(new Callback<Login>() {
            @Override
            public void onResponse(Call<Login> call, Response<Login> response) {
//                Log.d("res", response.body().getMessage());
                Log.d("res", new Gson().toJson(response.body()));
                if (response.body().getResult()) {
                    Karyawan usr = response.body().getData().get(0);
                    sp.setLogin(true);
                    db.addUser(usr.getNik(), usr.getNama());
                    Toast.makeText(getApplicationContext(),
                            "Halo,"+usr.getNama(), Toast.LENGTH_LONG).show();
                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    String errorMsg = response.body().getMessage();
                    Toast.makeText(getApplicationContext(),
                            errorMsg, Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<Login> call, Throwable t) {
                Log.d("err", String.valueOf(t));
            }
        });
    }
}
