package me.fianeka.workorder.Activity.OrderForm;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.firebase.messaging.FirebaseMessaging;

import java.util.ArrayList;
import java.util.List;

import me.fianeka.workorder.Activity.MainActivity;
import me.fianeka.workorder.Helper.DBHelper;
import me.fianeka.workorder.Model.ReturnRequest.Insert;
import me.fianeka.workorder.R;
import me.fianeka.workorder.Services.Api;
import me.fianeka.workorder.Services.RegisterApi;
import me.fianeka.workorder.Services.RetroHelper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FormPengadaan extends AppCompatActivity {
    private Toolbar tbar;
    private DBHelper db;
    private Spinner ruangans;
    private EditText jenis,jumlah, keterangan;
    private Button btn_order;
    private Snackbar snackbar;
    private ConstraintLayout root;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_pengadaan);
        root = findViewById(R.id.rootlayout);
        tbar = findViewById(R.id.toolbar);
        db = new DBHelper(this);
        set_action_bar();
        jumlah = findViewById(R.id.jumlah);
        jenis = findViewById(R.id.jenis);
        keterangan = findViewById(R.id.keterangan);
        btn_order = findViewById(R.id.btn_order);
        ruangans = findViewById(R.id.spinner);

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, getDataRuangan());
        ruangans.setAdapter(dataAdapter);

        btn_order.setOnClickListener(v -> {
            if (validateForm()) {
                storeDataOrder(ruangans.getSelectedItem().toString(), jenis.getText().toString(), jumlah.getText().toString(), keterangan.getText().toString());
            }
        });
    }

    private void storeDataOrder(String ruangan, String jenis, String jumlah, String keterangan) {
        String idsub = getIntent().getStringExtra("idsub");
        String idjen = getIntent().getStringExtra("idjenis");
        String ket = "Pesanan Pengadaan Untuk Ruangan: "+ruangan+", Dengan Jenis Pengadaan: "+jenis+", Jumlah Kebutuhan Sebanyak: "+jumlah+", Keterangan Tambahan: "+keterangan;
        Log.d("data", idsub + " " + idjen + " " + ket);
        RetroHelper help = new RetroHelper();
        RegisterApi res = help.getInstance().create(RegisterApi.class);
        Call<Insert> results = res.Order(Api.API_KEY, db.getNik(), idjen, ket, idsub);
        results.enqueue(new Callback<Insert>() {
            @Override
            public void onResponse(Call<Insert> call, Response<Insert> response) {
                final Insert myResponse = response.body();
                runOnUiThread(() -> {
                    try {
                        Log.d("res", myResponse.getMessage());
                        if (myResponse.getResult()) {
                            String[] parts =myResponse.getMessage().split("-");
                            Log.d("Subscribe To Firebase", "order"+parts[0]);
                            FirebaseMessaging.getInstance().subscribeToTopic("order"+parts[0]);
                            AlertDialog alertDialog = new AlertDialog.Builder(FormPengadaan.this).create();
                            alertDialog.setTitle("Selamat !!");
                            alertDialog.setMessage("Order sedang diproses, jika diperlukan silahkan hubungi SDM. Terimkasih.");
                            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Close",
                                    (dialog, which) -> {
                                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        startActivity(intent);
                                        dialog.dismiss();
                                    });
                            alertDialog.setCancelable(false);
                            alertDialog.show();
                        } else {
                            show_snackbar(myResponse.getMessage());
                        }
                    } catch (Exception ex) {
                        Log.d("Eror", ex.getMessage());
                        show_snackbar(ex.getMessage());
                    }
                });
            }

            @Override
            public void onFailure(Call<Insert> call, Throwable t) {
                show_snackbar(t.getMessage());

            }
        });
    }

    private boolean validateForm() {
        String jml = jumlah.getText().toString().trim();
        String jns = jenis.getText().toString().trim();
        String ktr = keterangan.getText().toString().trim();
        if (ruangans.getSelectedItemPosition()==0){
            show_snackbar("Infokan lokasi ruangan yang mebutuhkan");
            return false;
        }
        if (jns.isEmpty()) {
            show_snackbar("Infokan jenis pengadaan yang diajukan");
            return false;
        }
        if (jml.isEmpty()) {
            show_snackbar("Infokan jumlah pesanan yang dibutuhkan");
            return false;
        }
        if (ktr.isEmpty()) {
            show_snackbar("Infokan kami detail pesanan anda");
            return false;
        }
        return true;

    }

    private void show_snackbar(String pesan) {
        snackbar = Snackbar
                .make(root, pesan, Snackbar.LENGTH_INDEFINITE)
                .setAction("Close", view -> snackbar.dismiss());
        snackbar.setActionTextColor(ContextCompat.getColor(snackbar.getContext(), R.color.colorActiveSmall));
        View snackbarView = snackbar.getView();
        snackbarView.setBackgroundColor(Color.parseColor("#996b3a"));
        TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextSize(12);
        textView.setTextColor(Color.WHITE);
        snackbar.show();
    }

    private List<String> getDataRuangan() {
        List<String> tospinner = new ArrayList<>();
        String[] ruang = getResources().getStringArray(R.array.ruangan);
        for (String s:ruang) {
            tospinner.add(s);
        }
        tospinner.add(0, "Pilih Ruangan");
        tospinner.add("Lainnya");
        return tospinner;
    }

    private void set_action_bar() {
        setSupportActionBar(tbar);
        ActionBar ab = getSupportActionBar();
        getSupportActionBar().setTitle("Order Pengadaan");
        ab.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#996b3a"))); //without theme);
        TextView tv = new TextView(getApplicationContext());
        // Create a LayoutParams for TextView
        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT, // Width of TextView
                RelativeLayout.LayoutParams.WRAP_CONTENT); // Height of TextView

        // Apply the layout parameters to TextView widget
        tv.setLayoutParams(lp);

        // Set text to display in TextView
        tv.setText(ab.getTitle()); // ActionBar title text

        // Set the text color of TextView to black
        // This line change the ActionBar title text color
        tv.setTextColor(Color.WHITE);
        // Set the TextView text size in dp
        // This will change the ActionBar title text size
        tv.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 16);

        // Set the ActionBar display option
        ab.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);

        // Finally, set the newly created TextView as ActionBar custom view
        ab.setCustomView(tv);
        ab.setDisplayHomeAsUpEnabled(true);
        ab.setHomeAsUpIndicator(R.drawable.ic_back);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id==android.R.id.home){
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}
