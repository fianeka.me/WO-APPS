package me.fianeka.workorder.Activity.OrderForm;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.text.format.DateFormat;
import android.util.Log;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.firebase.messaging.FirebaseMessaging;
import com.takisoft.datetimepicker.DatePickerDialog;
import com.takisoft.datetimepicker.TimePickerDialog;

import java.util.Calendar;

import me.fianeka.workorder.Activity.MainActivity;
import me.fianeka.workorder.Helper.DBHelper;
import me.fianeka.workorder.Model.ReturnRequest.Insert;
import me.fianeka.workorder.R;
import me.fianeka.workorder.Services.Api;
import me.fianeka.workorder.Libs.DateFormater;
import me.fianeka.workorder.Services.RegisterApi;
import me.fianeka.workorder.Services.RetroHelper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FormTiket extends AppCompatActivity implements View.OnClickListener {

    /**
     * Init Variable
     **/
    private Toolbar tbar;
    private Snackbar snackbar;
    private DBHelper db;
    private LinearLayout layoutpulang;
    private CheckBox cekpulang;
    private EditText acara, haripergi, jampergi, haripulang, jampulang, jumlah, keterangan;
    private Calendar cal = Calendar.getInstance();
    private Button btn_order;
    private ConstraintLayout root;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_tiket);
        tbar = findViewById(R.id.toolbar);
        db = new DBHelper(this);
        root = findViewById(R.id.rootlayout);
        /**
         * Init Edit Text
         **/
        acara = findViewById(R.id.acara);
        haripergi = findViewById(R.id.haripergi);
        jampergi = findViewById(R.id.jampergi);
        haripulang = findViewById(R.id.haripulang);
        jampulang = findViewById(R.id.jampulang);
        jumlah = findViewById(R.id.jumlah);
        keterangan = findViewById(R.id.keterangan);
        btn_order = findViewById(R.id.btn_order);
        /**
         * Change Keyboard To Be Dialog
         **/
        haripergi.setInputType(InputType.TYPE_NULL);
        jampergi.setInputType(InputType.TYPE_NULL);
        haripulang.setInputType(InputType.TYPE_NULL);
        jampulang.setInputType(InputType.TYPE_NULL);
        haripergi.setOnClickListener(this);
        haripulang.setOnClickListener(this);
        jampergi.setOnClickListener(this);
        jampulang.setOnClickListener(this);
        /**
         * Layout And Cekbox
         **/
        layoutpulang = findViewById(R.id.layoutpulang);
        cekpulang = findViewById(R.id.pulangpergi);
        cekpulang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /** Gone Then Show, Another Gone **/
                if (layoutpulang.getVisibility() == View.GONE) {
                    layoutpulang.setVisibility(View.VISIBLE);
                } else {
                    layoutpulang.setVisibility(View.GONE);
                }
            }
        });
        btn_order.setOnClickListener(v -> {
            if (validateForm()) {
                storeDataOrder(acara.getText().toString(), jumlah.getText().toString(), keterangan.getText().toString());
            }
        });
        set_action_bar();
    }

    private void storeDataOrder(String acara, String jumlah, String keterangan) {
        String idsub = getIntent().getStringExtra("idsub");
        String idjen = getIntent().getStringExtra("idjenis");
        String pergi = DateFormater.doFormat("en",haripergi.getText().toString())+", "+jampergi.getText().toString();
        String pulang = DateFormater.doFormat("en",haripulang.getText().toString())+", "+jampulang.getText().toString();
        String ket;
        if(cekpulang.isChecked()) {
            ket = "Pesanan Tiket Utuk Acara: "+acara+", Tiket Untuk Pulang & Pergi: - Pergi:"+pergi+" - Pulang:"+pulang+" Jumlah Tiket Untuk: "+jumlah+"/Orang, Keterangan Tambahan: "+keterangan;
        }else{
            ket = "Pesanan Tiket Utuk Acara: "+acara+", Tiket Untuk Pergi Saja: - Pergi:"+pergi+" Jumlah Tiket Untuk: "+jumlah+"/Orang, Keterangan Tambahan: "+keterangan;
        }
        Log.d("data", idsub + " " + idjen + " " + ket);
        RetroHelper help = new RetroHelper();
        RegisterApi res = help.getInstance().create(RegisterApi.class);
        Call<Insert> results = res.Order(Api.API_KEY, db.getNik(), idjen, ket, idsub);
        results.enqueue(new Callback<Insert>() {
            @Override
            public void onResponse(Call<Insert> call, Response<Insert> response) {
                final Insert myResponse = response.body();
                runOnUiThread(() -> {
                    try {
                        Log.d("res", myResponse.getMessage());
                        if (myResponse.getResult()) {
                            String[] parts =myResponse.getMessage().split("-");
                            Log.d("Subscribe To Firebase", "order"+parts[0]);
                            FirebaseMessaging.getInstance().subscribeToTopic("order"+parts[0]);
                            AlertDialog alertDialog = new AlertDialog.Builder(FormTiket.this).create();
                            alertDialog.setTitle("Selamat !!");
                            alertDialog.setMessage("Order sedang diproses, jika diperlukan silahkan hubungi SDM. Terimkasih.");
                            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Close",
                                    (dialog, which) -> {
                                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                        startActivity(intent);
                                        dialog.dismiss();
                                    });
                            alertDialog.setCancelable(false);
                            alertDialog.show();
                        } else {
                            show_snackbar(myResponse.getMessage());
                        }
                    } catch (Exception ex) {
                        Log.d("Eror", ex.getMessage());
                        show_snackbar(ex.getMessage());
                    }
                });
            }

            @Override
            public void onFailure(Call<Insert> call, Throwable t) {
                show_snackbar(t.getMessage());

            }
        });
    }

    private void showDate(final EditText datetext) {
        DatePickerDialog dpd = new DatePickerDialog(this, (view1, year, month, dayOfMonth) -> {
            datetext.setText(String.format("%d", year) + "-" + String.format("%02d", month + 1) + "-" + String.format("%02d", dayOfMonth));
        }, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DATE));
        dpd.show();
    }

    private void showTime(final EditText datetext) {
        TimePickerDialog tpd = new TimePickerDialog(this, (view1, hourOfDay, minute) -> {
            datetext.setText(String.format("%02d", hourOfDay) + ":" + String.format("%02d", minute));
        }, cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), DateFormat.is24HourFormat(this));
        tpd.show();
    }

    private boolean validateForm() {
        String acaraval = acara.getText().toString().trim();
        String jml = jumlah.getText().toString().trim();
        String ktr = keterangan.getText().toString().trim();
        String jpergi = jampergi.getText().toString().trim();
        String jpulang = jampulang.getText().toString().trim();
        String hpergi = haripergi.getText().toString().trim();
        String hpulang = haripulang.getText().toString().trim();
        if (cekpulang.isChecked()) {
            if (jpulang.isEmpty() || hpulang.isEmpty() ) {
                show_snackbar("Hari Dan Jam kepulangan diperlukan keduannya");
                return false;
            }
        }
        if (acaraval.isEmpty()) {
            show_snackbar("Nama keperluan / acara diperlukan");
            return false;
        }
        if (jpergi.isEmpty() || hpergi.isEmpty() ) {
            show_snackbar("Hari Dan Jam kepergian diperlukan keduannya");
            return false;
        }
        if (jml.isEmpty()) {
            show_snackbar("Infokan jumlah pesanan yang dibutuhkan");
            return false;
        }
        if (ktr.isEmpty()) {
            show_snackbar("Infokan kami detail pesanan anda");
            return false;
        }
        return true;

    }

    private void show_snackbar(String pesan) {
        snackbar = Snackbar
                .make(root, pesan, Snackbar.LENGTH_INDEFINITE)
                .setAction("Close", view -> snackbar.dismiss());
        snackbar.setActionTextColor(ContextCompat.getColor(snackbar.getContext(), R.color.colorActiveSmall));
        View snackbarView = snackbar.getView();
        snackbarView.setBackgroundColor(Color.parseColor("#996b3a"));
        TextView textView = (TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextSize(12);
        textView.setTextColor(Color.WHITE);
        snackbar.show();
    }


    private void set_action_bar() {
        /**
         * Set For Custom Action Bar
         **/
        setSupportActionBar(tbar);
        ActionBar ab = getSupportActionBar();
        getSupportActionBar().setTitle("Order Tiket");
        ab.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#996b3a")));
        TextView tv = new TextView(getApplicationContext());
        // Create a LayoutParams for TextView
        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT);
        tv.setLayoutParams(lp);
        tv.setText(ab.getTitle());
        tv.setTextColor(Color.WHITE);
        tv.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 16);
        ab.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        ab.setCustomView(tv);
        ab.setDisplayHomeAsUpEnabled(true);
        ab.setHomeAsUpIndicator(R.drawable.ic_back);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == haripergi.getId()) {
            showDate(haripergi);
        } else if (v.getId() == haripulang.getId()) {
            showDate(haripulang);
        } else if (v.getId() == jampulang.getId()) {
            showTime(jampulang);
        } else if (v.getId() == jampergi.getId()) {
            showTime(jampergi);
        }
    }


}
