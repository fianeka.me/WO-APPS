package me.fianeka.workorder.Activity;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import me.fianeka.workorder.Helper.DBHelper;
import me.fianeka.workorder.Model.Data.HistoryOrder;
import me.fianeka.workorder.Model.Karyawan.Karyawan;
import me.fianeka.workorder.Model.ReturnRequest.HisOrder;
import me.fianeka.workorder.R;
import me.fianeka.workorder.Services.Api;
import me.fianeka.workorder.Libs.CircleTranform;
import me.fianeka.workorder.Services.RegisterApi;
import me.fianeka.workorder.Services.RetroHelper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailOrder extends AppCompatActivity {
    private Toolbar tbar;
    private DBHelper db;
    private TextView jenis,keterangan,tgl_pesan,status,nomer;
    private TextView nik,nama,jabatan,call;
    private ImageView avakordinator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_order);
        tbar = findViewById(R.id.toolbar);
        db = new DBHelper(this);
        jenis = findViewById(R.id.jenis);
        keterangan = findViewById(R.id.keterangan);
        tgl_pesan = findViewById(R.id.tgl_pesan);
        status = findViewById(R.id.status);
        nomer = findViewById(R.id.nomer);
        nik = findViewById(R.id.nik);
        nama = findViewById(R.id.nama);
        jabatan = findViewById(R.id.jabatan);
        call = findViewById(R.id.call);
        avakordinator = findViewById(R.id.avakordinator);
        String idorder = getIntent().getStringExtra("id");
        getDetail(db.getNik(),idorder);
        set_action_bar();
    }

    private void getDetail(String nik, String idorder) {
        RetroHelper help = new RetroHelper();
        RegisterApi res = help.getInstance().create(RegisterApi.class);
        Call<HisOrder> results = res.OrderDetail(Api.API_KEY, nik, idorder);
        results.enqueue(new Callback<HisOrder>() {
            @Override
            public void onResponse(Call<HisOrder> call, Response<HisOrder> response) {
                final HisOrder myResponse = response.body();
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
//                            pgitem.setVisibility(View.GONE);
                            try {
                                HistoryOrder data = myResponse.getData().get(0);
                                Log.d("res", myResponse.getMessage());
                                if (myResponse.getResult()) {
                                  nomer.setText("NO. ORDER:ORD-"+data.getIdorder());
                                  status.setText(data.getStatus());
                                  if (data.getStatus().equals("Pending")||data.getStatus().equals("Ditolak")){
                                      status.setTextColor(Color.parseColor("#F63D2B"));
                                  }
                                    jenis.setText(data.getNamajenisorder());
                                    tgl_pesan.setText(data.getTglpemesanan());
                                    keterangan.setText(data.getKeterangan());
                                    setProfile(data.getKordinator().get(0));
                                } else {
                                    String errorMsg = myResponse.getMessage();
                                    Toast.makeText(DetailOrder.this, myResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            }catch (Exception ex){
                                Toast.makeText(DetailOrder.this, ex.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    });


            }

            @Override
            public void onFailure(Call<HisOrder> call, Throwable t) {
                Log.d("error", t.getMessage());
            }
        });
    }

    private void setProfile(final Karyawan karyawan) {
        Glide.with(DetailOrder.this).load(karyawan.getPhoto())
                .crossFade()
                .thumbnail(0.5f)
                .bitmapTransform(new CircleTranform(DetailOrder.this))
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(avakordinator);
        nik.setText(karyawan.getNik());
        nama.setText(karyawan.getNama());
        jabatan.setText(karyawan.getJabatan()+" "+karyawan.getSubdivisi());
        String[] name = karyawan.getNama().split(" ");
        call.setText("Hubungi "+name[0]);
        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:"+karyawan.getNotel()));
                startActivity(intent);
            }
        });
    }

    private void set_action_bar() {
        setSupportActionBar(tbar);
        ActionBar ab = getSupportActionBar();
        getSupportActionBar().setTitle("Detail Order");
        ab.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#996b3a"))); //without theme);
        TextView tv = new TextView(getApplicationContext());
        // Create a LayoutParams for TextView
        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT, // Width of TextView
                RelativeLayout.LayoutParams.WRAP_CONTENT); // Height of TextView

        // Apply the layout parameters to TextView widget
        tv.setLayoutParams(lp);

        // Set text to display in TextView
        tv.setText(ab.getTitle()); // ActionBar title text

        // Set the text color of TextView to black
        // This line change the ActionBar title text color
        tv.setTextColor(Color.WHITE);
        // Set the TextView text size in dp
        // This will change the ActionBar title text size
        tv.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 16);

        // Set the ActionBar display option
        ab.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);

        // Finally, set the newly created TextView as ActionBar custom view
        ab.setCustomView(tv);
        ab.setDisplayHomeAsUpEnabled(true);
        ab.setHomeAsUpIndicator(R.drawable.ic_back);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id==android.R.id.home){
            if (getIntent().getStringExtra("fire")!=null){
                startActivity(new Intent(this,MainActivity.class));
                finish();
            }
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        finish();
    }

}
