package me.fianeka.workorder.Activity;

import android.app.AlertDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.InputFilter;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import me.fianeka.workorder.Activity.OrderForm.FormKendaraan;
import me.fianeka.workorder.Adapter.AtkAdapter;
import me.fianeka.workorder.Helper.DBHelper;
import me.fianeka.workorder.Model.Data.Barang;
import me.fianeka.workorder.Model.ReturnRequest.Insert;
import me.fianeka.workorder.Model.ReturnRequest.JenBarang;
import me.fianeka.workorder.R;
import me.fianeka.workorder.Services.Api;
import me.fianeka.workorder.Libs.InputFilterMinMax;
import me.fianeka.workorder.Services.RegisterApi;
import me.fianeka.workorder.Services.RetroHelper;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BarangActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener, SearchView.OnCloseListener, SearchView.OnQueryTextListener, AtkAdapter.KlikBarang {
    private Toolbar tbar;
    private DBHelper db;
    private SwipeRefreshLayout swiper;
    private String idkategori;
    private SearchView searchView;
    private ListView list_barang;
    private AtkAdapter listAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_barang);
        tbar = findViewById(R.id.toolbar);
        db = new DBHelper(this);
        set_action_bar();
        idkategori = getIntent().getStringExtra("idkat");
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) findViewById(R.id.search);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName
                ()));
        searchView.setIconifiedByDefault(false);
        searchView.setOnQueryTextListener(this);
        searchView.setOnCloseListener(this);
        searchView.clearFocus();
        InputMethodManager mgr = (InputMethodManager) getSystemService(Context
                .INPUT_METHOD_SERVICE);
        mgr.hideSoftInputFromWindow(searchView.getWindowToken(), 0);

        list_barang = (ListView) findViewById(R.id.list_barang);
        swiper = findViewById(R.id.swiper);
        swiper.setOnRefreshListener(this);
        swiper.setColorSchemeResources(
                R.color.colorPrimary,
                R.color.colorAccent,
                R.color.colorPrimary);
        swiper.post(() -> {
            swiper.setRefreshing(true);
            getDataBarang(idkategori);
        }
        );
    }

    private void getDataBarang(String idkategori) {
        RetroHelper help = new RetroHelper();
        RegisterApi res = help.getInstance().create(RegisterApi.class);
        Call<JenBarang> results = res.BarangAtk(Api.API_KEY, idkategori);
        results.enqueue(new Callback<JenBarang>() {
            @Override
            public void onResponse(Call<JenBarang> call, Response<JenBarang> response) {
                final JenBarang myResponse = response.body();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        swiper.setRefreshing(false);
                        try {
                            Log.d("res", myResponse.getMessage());
                            if (myResponse.getResult()) {
                                final ArrayList<Barang> barangs = new ArrayList<Barang>(myResponse.getData());
                                listAdapter = new AtkAdapter(BarangActivity.this, myResponse.getData());
                                listAdapter.setKlikKategori(BarangActivity.this);
                                list_barang.setAdapter(listAdapter);
                            } else {
                                String errorMsg = myResponse.getMessage();
                                Toast.makeText(BarangActivity.this, errorMsg, Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception ex) {
                            Log.d("Eror", ex.getMessage());
                            Toast.makeText(BarangActivity.this, ex.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }
                });
            }

            @Override
            public void onFailure(Call<JenBarang> call, Throwable t) {
                Log.d("error", t.getMessage());
            }
        });
    }

    private void insertBon(String idatk, String jumlah) {
        RetroHelper help = new RetroHelper();
        RegisterApi res = help.getInstance().create(RegisterApi.class);
        Call<Insert> results = res.Bon(Api.API_KEY, db.getNik(), jumlah, idatk);
        results.enqueue(new Callback<Insert>() {
            @Override
            public void onResponse(Call<Insert> call, Response<Insert> response) {
                final Insert myResponse = response.body();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            Log.d("res", myResponse.getMessage());
                            if (myResponse.getResult()) {
                                android.support.v7.app.AlertDialog alertDialog = new android.support.v7.app.AlertDialog.Builder(BarangActivity.this).create();
                                alertDialog.setTitle("Selamat");
                                alertDialog.setMessage(myResponse.getMessage());
                                alertDialog.setButton(android.support.v7.app.AlertDialog.BUTTON_NEUTRAL, "Close",
                                        (dialog, which) -> {
                                            dialog.dismiss();
                                            swiper.setRefreshing(false);
                                            getDataBarang(idkategori);
                                        });
                                alertDialog.setCancelable(false);
                                alertDialog.show();
//                                Toast.makeText(BarangActivity.this, myResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            } else {
                                String errorMsg = myResponse.getMessage();
                                Toast.makeText(BarangActivity.this, errorMsg, Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception ex) {
                            Log.d("Eror", ex.getMessage());
                            Toast.makeText(BarangActivity.this, ex.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }
                });
            }

            @Override
            public void onFailure(Call<Insert> call, Throwable t) {
                Log.d("error", t.getMessage());
            }
        });
    }

    private void showDialog(final Barang barang) {
        LayoutInflater li = LayoutInflater.from(BarangActivity.this);
        View promptsView = li.inflate(R.layout.layout_bon, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setView(promptsView);
        final EditText jumlah = (EditText) promptsView.findViewById(R.id.jumlah);
        final TextView stok = promptsView.findViewById(R.id.sisastok);
        final Button btn_save = promptsView.findViewById(R.id.btn_save);
        stok.setText("Sisa Stok: " + barang.getStok());
        jumlah.setFilters(new InputFilter[]{new InputFilterMinMax("1", barang.getStok())});
//        nama.setText(barang.getNamabarang());
        alertDialogBuilder
                .setTitle(barang.getNamabarang())
                .setCancelable(false)
                .setNegativeButton("Kembali",
                        (dialog, id) -> dialog.cancel());
        AlertDialog alertDialog = alertDialogBuilder.create();
        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (jumlah.getText().toString().equals("")) {
                    Toast.makeText(BarangActivity.this, "Jumlah Pengambilan Diperlukan", Toast.LENGTH_SHORT).show();
                } else {
                    insertBon(barang.getIdatk(), jumlah.getText().toString());
                    alertDialog.cancel();
                }
            }
        });
        alertDialog.show();
    }

    @Override
    public void onRefresh() {
        getDataBarang(idkategori);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void set_action_bar() {
        setSupportActionBar(tbar);
        ActionBar ab = getSupportActionBar();
        if (getIntent().hasExtra("title")) {
            getSupportActionBar().setTitle("Kategori : " + getIntent().getStringExtra("title"));
        } else {
            getSupportActionBar().setTitle("Semua Kategori");
        }
        ab.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#996b3a"))); //without theme);
        TextView tv = new TextView(getApplicationContext());
        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT, // Width of TextView
                RelativeLayout.LayoutParams.WRAP_CONTENT); // Height of TextView
        tv.setLayoutParams(lp);
        tv.setText(ab.getTitle()); // ActionBar title text
        tv.setTextColor(Color.WHITE);
        tv.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 16);
        ab.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        ab.setCustomView(tv);
        ab.setDisplayHomeAsUpEnabled(true);
        ab.setHomeAsUpIndicator(R.drawable.ic_back);
    }

    @Override
    public boolean onClose() {
        return false;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        searchView.clearFocus();
        return false;
    }

    @Override
    public boolean onQueryTextChange(String s) {
        if (TextUtils.isEmpty(s)) {
            ((AtkAdapter) list_barang.getAdapter()).filter("");
        } else {
            ((AtkAdapter) list_barang.getAdapter()).filter(s);
        }
        return false;
    }

    @Override
    public void onKlikKategori(Barang selected) {
        if (!selected.getStok().equals("0")) {
            showDialog(selected);
        } else {
            Toast.makeText(this, "Barang "+selected.getNamabarang()+" Kehabisan Stok", Toast.LENGTH_SHORT).show();
        }
    }
}
