package me.fianeka.workorder.Adapter;

import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import me.fianeka.workorder.Model.Data.HistoryBon;
import me.fianeka.workorder.R;
import me.fianeka.workorder.Libs.DateFormater;

/**
 * Created by OWL on 20/06/2016.
 */
public class HistoryBonAdapter extends RecyclerView.Adapter<HistoryBonAdapter.BarangViewHolder> {
    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    private List<HistoryBon> cls_data;
    private FragmentActivity ctx;
    private ArrayList<Double> total = new ArrayList<>();
    private View v;

    public HistoryBonAdapter(FragmentActivity ctx, List<HistoryBon> class_data) {
        this.cls_data = class_data;
        this.ctx = ctx;
    }

    @Override
    public void onBindViewHolder(BarangViewHolder BarangViewHolder, int i) {
        final HistoryBon cls = cls_data.get(i);
        BarangViewHolder.tgl.setText(DateFormater.doFormat("in", cls.getTglpinjam()));
        BarangViewHolder.namabarang.setText(cls.getNamabarang());
        BarangViewHolder.jumlah.setText("Jumlah Bon: "+cls.getJumlahpinjam());


    }


    class BarangViewHolder extends RecyclerView.ViewHolder {
        TextView tgl, namabarang, jumlah;

        BarangViewHolder(View itemView) {
            super(itemView);
            tgl = itemView.findViewById(R.id.tgl);
            namabarang = (TextView) itemView.findViewById(R.id.namabarang);
            jumlah = (TextView) itemView.findViewById(R.id.jumlah);
        }
    }

    @Override
    public int getItemCount() {
        return cls_data.size();
    }

    @Override
    public BarangViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.template_hisbon, viewGroup, false);
        BarangViewHolder pvh = new BarangViewHolder(v);
        return pvh;
    }


}