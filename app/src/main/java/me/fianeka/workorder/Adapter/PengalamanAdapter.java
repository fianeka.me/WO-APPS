package me.fianeka.workorder.Adapter;

import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import me.fianeka.workorder.Model.Karyawan.Kursus;
import me.fianeka.workorder.Model.Karyawan.Pengalaman;
import me.fianeka.workorder.R;

/**
 * Created by OWL on 20/06/2016.
 */
public class PengalamanAdapter extends RecyclerView.Adapter<PengalamanAdapter.BarangViewHolder> {
    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    private List<Pengalaman> cls_data;
    private FragmentActivity ctx;
    private ArrayList<Double> total=new ArrayList<>();
    private View v;
    public PengalamanAdapter(FragmentActivity ctx, List<Pengalaman> class_data){
        this.cls_data = class_data;
        this.ctx=ctx;
    }

    @Override
    public void onBindViewHolder(BarangViewHolder BarangViewHolder, int i) {
        Pengalaman cls= cls_data.get(i);
//        Log.d("Data Masuk", cls.getPendidikan()+" "+cls.getInstitusi()+" "+cls.getTahun());
        BarangViewHolder.jabatan.setText(cls.getJabatan());
        BarangViewHolder.institusi.setText(cls.getNamainstitusi());
        BarangViewHolder.tahun.setText(cls.getTahun());
    }


    class BarangViewHolder extends RecyclerView.ViewHolder{
        TextView jabatan,institusi,tahun;
        BarangViewHolder(View itemView) {
            super(itemView);
            jabatan=(TextView) itemView.findViewById(R.id.jabatan);
            institusi=(TextView) itemView.findViewById(R.id.institusi);
            tahun= (TextView) itemView.findViewById(R.id.tahun);
        }
    }
    @Override
    public int getItemCount() {
        return cls_data.size();
    }

    @Override
    public BarangViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.template_pengalaman, viewGroup, false);
        BarangViewHolder pvh = new BarangViewHolder(v);
        return pvh;
    }



}