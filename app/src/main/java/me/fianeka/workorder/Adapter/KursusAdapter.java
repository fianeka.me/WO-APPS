package me.fianeka.workorder.Adapter;

import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import me.fianeka.workorder.Model.Karyawan.Kursus;
import me.fianeka.workorder.Model.Karyawan.Riwayat;
import me.fianeka.workorder.R;

/**
 * Created by OWL on 20/06/2016.
 */
public class KursusAdapter extends RecyclerView.Adapter<KursusAdapter.BarangViewHolder> {
    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    private List<Kursus> cls_data;
    private FragmentActivity ctx;
    private ArrayList<Double> total=new ArrayList<>();
    private View v;
    public KursusAdapter(FragmentActivity ctx, List<Kursus> class_data){
        this.cls_data = class_data;
        this.ctx=ctx;
    }

    @Override
    public void onBindViewHolder(BarangViewHolder BarangViewHolder, int i) {
        Kursus cls= cls_data.get(i);
//        Log.d("Data Masuk", cls.getPendidikan()+" "+cls.getInstitusi()+" "+cls.getTahun());
        BarangViewHolder.tema.setText(cls.getTema());
        BarangViewHolder.penyelenggara.setText(cls.getPenyelenggara());
        BarangViewHolder.tahun.setText(cls.getTglkursus());
    }


    class BarangViewHolder extends RecyclerView.ViewHolder{
        TextView tema,penyelenggara,tahun;
        BarangViewHolder(View itemView) {
            super(itemView);
            tema=(TextView) itemView.findViewById(R.id.tema);
            penyelenggara=(TextView) itemView.findViewById(R.id.penyelenggara);
            tahun= (TextView) itemView.findViewById(R.id.tahun);
        }
    }
    @Override
    public int getItemCount() {
        return cls_data.size();
    }

    @Override
    public BarangViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.template_kursus, viewGroup, false);
        BarangViewHolder pvh = new BarangViewHolder(v);
        return pvh;
    }



}