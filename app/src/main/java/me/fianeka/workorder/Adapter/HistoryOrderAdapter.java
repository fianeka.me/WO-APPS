package me.fianeka.workorder.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import me.fianeka.workorder.Activity.DetailOrder;
import me.fianeka.workorder.Model.Data.HistoryOrder;
import me.fianeka.workorder.R;
import me.fianeka.workorder.Libs.DateFormater;

/**
 * Created by OWL on 20/06/2016.
 */
public class HistoryOrderAdapter extends RecyclerView.Adapter<HistoryOrderAdapter.BarangViewHolder> {
    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    private List<HistoryOrder> cls_data;
    private FragmentActivity ctx;
    private ArrayList<Double> total=new ArrayList<>();
    private View v;
    public HistoryOrderAdapter(FragmentActivity ctx, List<HistoryOrder> class_data){
        this.cls_data = class_data;
        this.ctx=ctx;
    }

    public static int getImageId(Context context, String imageName) {
        return context.getResources().getIdentifier("drawable/" + imageName, null, context.getPackageName());
    }

    @Override
    public void onBindViewHolder(BarangViewHolder BarangViewHolder, int i) {
        final HistoryOrder cls= cls_data.get(i);
        BarangViewHolder.jenis.setText(cls.getNamajenisorder());
        BarangViewHolder.status.setText(cls.getStatus());
        if (cls.getStatus().equals("Pending") || cls.getStatus().equals("Ditolak")){
            BarangViewHolder.status.setTextColor(Color.parseColor("#F63D2B"));
        }
        BarangViewHolder.keterangan.setText(cls.getKeterangan());
        String[] sdate = cls.getTglpemesanan().split(" ");
        BarangViewHolder.tgl_pesan.setText(DateFormater.doFormat("en", sdate[0])+" Jam: "+sdate[1]);
        String[] favicon = cls.getFavicon().split("-");
        String ic = "ic_"+favicon[1];
        BarangViewHolder.icon.setImageResource(getImageId(ctx,ic));
        BarangViewHolder.detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ctx, DetailOrder.class);
                intent.putExtra("id", cls.getIdorder());
                ctx.startActivity(intent);

            }
        });
    }


    class BarangViewHolder extends RecyclerView.ViewHolder{
        TextView keterangan,tgl_pesan,tahun,status,jenis,detail;
        ImageView icon;
        BarangViewHolder(View itemView) {
            super(itemView);
            icon = itemView.findViewById(R.id.icon);
            jenis=(TextView) itemView.findViewById(R.id.jenis);
            status=(TextView) itemView.findViewById(R.id.status);
            keterangan= (TextView) itemView.findViewById(R.id.keterangan);
            tgl_pesan= (TextView) itemView.findViewById(R.id.tgl_pesan);
            detail = (TextView) itemView.findViewById(R.id.detail);
        }
    }
    @Override
    public int getItemCount() {
        return cls_data.size();
    }

    @Override
    public BarangViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.template_hisorder, viewGroup, false);
        BarangViewHolder pvh = new BarangViewHolder(v);
        return pvh;
    }



}