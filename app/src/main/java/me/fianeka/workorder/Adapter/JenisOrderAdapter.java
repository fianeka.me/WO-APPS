package me.fianeka.workorder.Adapter;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import me.fianeka.workorder.Model.Data.JenisOrder;
import me.fianeka.workorder.R;

/**
 * Created by OWL on 20/06/2016.
 */
public class JenisOrderAdapter extends RecyclerView.Adapter<JenisOrderAdapter.BarangViewHolder> {
    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    private List<JenisOrder> cls_data;
    private FragmentActivity ctx;
    private ArrayList<Double> total = new ArrayList<>();
    private View v;
    private static JenisOrderAdapter.KlikOrder klikOrder;


    public JenisOrderAdapter(FragmentActivity ctx, List<JenisOrder> class_data) {
        this.cls_data = class_data;
        this.ctx = ctx;
    }

    public static int getImageId(Context context, String imageName) {
        return context.getResources().getIdentifier("drawable/" + imageName, null, context.getPackageName());
    }

    @Override
    public void onBindViewHolder(BarangViewHolder BarangViewHolder, int i) {
        final JenisOrder cls = cls_data.get(i);
        String[] favicon = cls.getFavicon().split("-");
        String ic = "ic_"+favicon[1];
        BarangViewHolder.icon.setImageResource(getImageId(ctx,ic));
        BarangViewHolder.jenis.setText(cls.getNamajenis());
        BarangViewHolder.subdiv.setText(cls.getNamasubdiv());
        BarangViewHolder.orderaction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JenisOrderAdapter.klikOrder.onKlikOrder(cls);

            }
        });
    }


    class BarangViewHolder extends RecyclerView.ViewHolder {
        TextView jenis, subdiv;
        ImageView icon;
        LinearLayout orderaction;
        BarangViewHolder(View itemView) {
            super(itemView);
            jenis = itemView.findViewById(R.id.jenis);
            subdiv = (TextView) itemView.findViewById(R.id.subdiv);
            icon = itemView.findViewById(R.id.icon);
            orderaction = itemView.findViewById(R.id.orderaction);
        }
    }

    public interface KlikOrder {
        void onKlikOrder(JenisOrder selected);
    }

    public void setKlikOrder(JenisOrderAdapter.KlikOrder hitung) {
        JenisOrderAdapter.klikOrder = hitung;
    }

    @Override
    public int getItemCount() {
        return cls_data.size();
    }

    @Override
    public BarangViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.template_jenisorder, viewGroup, false);
        BarangViewHolder pvh = new BarangViewHolder(v);
        return pvh;
    }

}