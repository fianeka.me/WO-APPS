package me.fianeka.workorder.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import me.fianeka.workorder.Model.Data.Barang;
import me.fianeka.workorder.R;

public class AtkAdapter extends BaseAdapter {

    // Declare Variables
    Context mContext;
    LayoutInflater inflater;
    private List<Barang> barang = null;
    private ArrayList<Barang> arraylist;
    private static KlikBarang klikBarang;

    public AtkAdapter(Context context, List<Barang> barangs) {
        mContext = context;
        this.barang = barangs;
        inflater = LayoutInflater.from(mContext);
        this.arraylist = new ArrayList<Barang>();
        this.arraylist.addAll(barangs);
    }

    public class ViewHolder {
        TextView nama,sisastok,kategori;
        LinearLayout mylayout;
    }

    @Override
    public int getCount() {
        return barang.size();
    }

    @Override
    public Barang getItem(int position) {
        return barang.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View view, ViewGroup parent) {
        final ViewHolder holder;
        final Barang cls = barang.get(position);
        if (view == null) {
            holder = new ViewHolder();
            view = inflater.inflate(R.layout.template_atk, null);
            holder.nama= (TextView) view.findViewById(R.id.namabarang);
            holder.sisastok = (TextView) view.findViewById(R.id.sisastok);
            holder.kategori = (TextView) view.findViewById(R.id.kategori);
            holder.mylayout = view.findViewById(R.id.mylayout);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        holder.nama.setText(cls.getNamabarang());
        holder.sisastok.setText("Sisa Stok : "+cls.getStok());
        holder.kategori.setText(cls.getKategori());
        holder.mylayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AtkAdapter.klikBarang.onKlikKategori(cls);
            }
        });
        return view;
    }

    public interface KlikBarang {
        void onKlikKategori(Barang selected);
    }

    public void setKlikKategori(KlikBarang hitung) {
        AtkAdapter.klikBarang = hitung;
    }

    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        barang.clear();
        if (charText.length() == 0) {
            barang.addAll(arraylist);
        }
        else
        {
            for (Barang wp : arraylist)
            {
                if (wp.getNamabarang().toLowerCase(Locale.getDefault()).contains(charText)||wp.getKategori().toLowerCase(Locale.getDefault()).contains(charText))
                {
                    barang.add(wp);
                }
            }
        }
        notifyDataSetChanged();
    }

}