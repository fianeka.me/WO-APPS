package me.fianeka.workorder.Adapter;

import android.content.Intent;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import me.fianeka.workorder.Activity.JenisOrder;
import me.fianeka.workorder.R;
import me.fianeka.workorder.Model.Data.SubdivisiOrder;

/**
 * Created by OWL on 20/06/2016.
 */
public class SubdivOrderAdapter extends RecyclerView.Adapter<SubdivOrderAdapter.BarangViewHolder> {
    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    private List<SubdivisiOrder> cls_data;
    private FragmentActivity ctx;
    private ArrayList<Double> total=new ArrayList<>();
    private View v;
    public SubdivOrderAdapter(FragmentActivity ctx, List<SubdivisiOrder> class_data){
        this.cls_data = class_data;
        this.ctx=ctx;
    }

    @Override
    public void onBindViewHolder(BarangViewHolder BarangViewHolder, int i) {
        final SubdivisiOrder cls= cls_data.get(i);
        Log.d("coba", cls.getPhoto());
        Glide.with(v.getContext()).load(cls.getPhoto()).thumbnail(0.5f)
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.ALL).into(BarangViewHolder.img1);
        BarangViewHolder.namasubdiv.setText(cls.getSubdivisi());
        BarangViewHolder.keterangansubdiv.setText(cls.getKeterangan());
        BarangViewHolder.selengkapnya.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mainIntent = new Intent(ctx,JenisOrder.class);
                mainIntent.putExtra("idsub",cls.getIdsubdiv());
                mainIntent.putExtra("title",cls.getSubdivisi());
                ctx.startActivity(mainIntent);
            }
        });
    }


    class BarangViewHolder extends RecyclerView.ViewHolder{
        TextView namasubdiv,keterangansubdiv;
        ImageView img1;
        LinearLayout selengkapnya;
        BarangViewHolder(View itemView) {
            super(itemView);
            namasubdiv=(TextView) itemView.findViewById(R.id.namasubdiv);
            keterangansubdiv=(TextView) itemView.findViewById(R.id.keterangansubdiv);
            img1= (ImageView) itemView.findViewById(R.id.img1);
            selengkapnya = itemView.findViewById(R.id.selengkapnya);
        }
    }
    @Override
    public int getItemCount() {
        return cls_data.size();
    }

    @Override
    public BarangViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.template_subdivorder, viewGroup, false);
        BarangViewHolder pvh = new BarangViewHolder(v);
        return pvh;
    }



}