package me.fianeka.workorder.Adapter;

import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;
import java.util.List;

import me.fianeka.workorder.Model.Data.KategoriBarang;
import me.fianeka.workorder.Model.Karyawan.Riwayat;
import me.fianeka.workorder.R;

/**
 * Created by OWL on 20/06/2016.
 */
public class RiwayatAdapter extends RecyclerView.Adapter<RiwayatAdapter.BarangViewHolder> {
    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    private List<Riwayat> cls_data;
    private FragmentActivity ctx;
    private ArrayList<Double> total=new ArrayList<>();
    private View v;
    public RiwayatAdapter(FragmentActivity ctx, List<Riwayat> class_data){
        this.cls_data = class_data;
        this.ctx=ctx;
    }

    @Override
    public void onBindViewHolder(BarangViewHolder BarangViewHolder, int i) {
        Riwayat cls= cls_data.get(i);
        Log.d("Data Masuk", cls.getPendidikan()+" "+cls.getInstitusi()+" "+cls.getTahun());
        BarangViewHolder.pendidikan.setText(cls.getPendidikan());
        BarangViewHolder.institusi.setText(cls.getInstitusi());
        BarangViewHolder.tahun.setText(cls.getTahun());
    }


    class BarangViewHolder extends RecyclerView.ViewHolder{
        TextView pendidikan,institusi,tahun;
        BarangViewHolder(View itemView) {
            super(itemView);
            pendidikan=(TextView) itemView.findViewById(R.id.pendidikan);
            institusi=(TextView) itemView.findViewById(R.id.institusi);
            tahun= (TextView) itemView.findViewById(R.id.tahun);
        }
    }
    @Override
    public int getItemCount() {
        return cls_data.size();
    }

    @Override
    public BarangViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.template_riwayat, viewGroup, false);
        BarangViewHolder pvh = new BarangViewHolder(v);
        return pvh;
    }



}