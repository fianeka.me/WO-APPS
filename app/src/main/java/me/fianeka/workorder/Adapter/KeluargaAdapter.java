package me.fianeka.workorder.Adapter;

import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import me.fianeka.workorder.Model.Karyawan.Keluarga;
import me.fianeka.workorder.Model.Karyawan.Riwayat;
import me.fianeka.workorder.R;

/**
 * Created by OWL on 20/06/2016.
 */
public class KeluargaAdapter extends RecyclerView.Adapter<KeluargaAdapter.BarangViewHolder> {
    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    private List<Keluarga> cls_data;
    private FragmentActivity ctx;
    private ArrayList<Double> total=new ArrayList<>();
    private View v;
    public KeluargaAdapter(FragmentActivity ctx, List<Keluarga> class_data){
        this.cls_data = class_data;
        this.ctx=ctx;
    }

    @Override
    public void onBindViewHolder(BarangViewHolder BarangViewHolder, int i) {
        Keluarga cls= cls_data.get(i);
        BarangViewHolder.nama.setText(cls.getNama());
        String jekel = cls.getJenkel().equals("L") ? "Laki-Laki" : "Perempuan";
        BarangViewHolder.jekel.setText(jekel);
        BarangViewHolder.alamat.setText(cls.getAlamat());
        BarangViewHolder.hub.setText(cls.getHubungan());
        BarangViewHolder.tgl.setText(cls.getTgllahir());
        BarangViewHolder.pekerjaan.setText(cls.getPekerjaan());
    }


    class BarangViewHolder extends RecyclerView.ViewHolder{
        TextView nama,hub,tgl,jekel,alamat,pekerjaan;
        BarangViewHolder(View itemView) {
            super(itemView);
            nama=(TextView) itemView.findViewById(R.id.nama);
            hub=(TextView) itemView.findViewById(R.id.hub);
            tgl= (TextView) itemView.findViewById(R.id.tgl);
            jekel= (TextView) itemView.findViewById(R.id.jekel);
            alamat= (TextView) itemView.findViewById(R.id.alamat);
            pekerjaan= (TextView) itemView.findViewById(R.id.pekerjaan);
        }
    }
    @Override
    public int getItemCount() {
        return cls_data.size();
    }

    @Override
    public BarangViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.template_keluarga, viewGroup, false);
        BarangViewHolder pvh = new BarangViewHolder(v);
        return pvh;
    }



}