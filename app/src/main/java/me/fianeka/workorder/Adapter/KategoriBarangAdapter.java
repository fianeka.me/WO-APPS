package me.fianeka.workorder.Adapter;

import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;
import java.util.List;

import me.fianeka.workorder.Activity.BarangActivity;
import me.fianeka.workorder.Model.Data.KategoriBarang;
import me.fianeka.workorder.Model.Data.SubdivisiOrder;
import me.fianeka.workorder.R;

/**
 * Created by OWL on 20/06/2016.
 */
public class KategoriBarangAdapter extends RecyclerView.Adapter<KategoriBarangAdapter.BarangViewHolder> {
    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    private List<KategoriBarang> cls_data;
    private FragmentActivity ctx;
    private ArrayList<Double> total=new ArrayList<>();
    private View v;
    public KategoriBarangAdapter(FragmentActivity ctx, List<KategoriBarang> class_data){
        this.cls_data = class_data;
        this.ctx=ctx;
    }

    @Override
    public void onBindViewHolder(BarangViewHolder BarangViewHolder, int i) {
        final KategoriBarang cls= cls_data.get(i);
        Glide.with(v.getContext()).load(cls.getPhoto()).thumbnail(0.5f)
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.ALL).into(BarangViewHolder.img1);
        BarangViewHolder.namakat.setText(cls.getKategori());
        BarangViewHolder.jmlbrng.setText("Terdapat "+cls.getTotal()+" Barang");
        BarangViewHolder.img1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mainIntent = new Intent(ctx,BarangActivity.class);
                mainIntent.putExtra("idkat",cls.getIdkat());
                mainIntent.putExtra("title",cls.getKategori());
                ctx.startActivity(mainIntent);
            }
        });
    }


    class BarangViewHolder extends RecyclerView.ViewHolder{
        TextView namakat,jmlbrng;
        ImageView img1;
        BarangViewHolder(View itemView) {
            super(itemView);
            namakat=(TextView) itemView.findViewById(R.id.namakat);
            jmlbrng=(TextView) itemView.findViewById(R.id.jmlbrng);
            img1= (ImageView) itemView.findViewById(R.id.img1);
        }
    }
    @Override
    public int getItemCount() {
        return cls_data.size();
    }

    @Override
    public BarangViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.template_kategori, viewGroup, false);
        BarangViewHolder pvh = new BarangViewHolder(v);
        return pvh;
    }



}